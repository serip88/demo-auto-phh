//đây sẽ xử lý giảm 3 lần liên tiếp sẽ mua nếu gia tốc đạt yêu cầu


const ccxt = require("ccxt");
const moment = require("moment");
const dalay = require('delay');
const config = require('./item-config.json');
const data = require('./data.json');
var {appDir} = require('../main');
const fs = require("fs");

const binance = new ccxt.binance({
  apiKey: 'YAlGLKtCOsQybLB9vZ59vxmHJ17JswdTU2qw00HY3Y3Ks7cGqQUDxcPy0OCeltQl',
  secret: 'WTyHqNygotrAYmKRZa0iWmx7M2KkTTSowanjH8jeFdQTv2aLTOyWLeer2rv9UzkM'
});
binance.setSandboxMode(true)


const TRADE_SIZE = config.TRADE_SIZE
const pairs = config.pairs
const tpMin = config.tpMin
const tpMax = config.tpMax
const stopLoss = config.stopLoss

let progress = data.progress
let highPrice = data.highPrice
let lowPrice = data.lowPrice
let currentPrice = data.currentPrice
let currentChart = data.currentChart
let priceSL = data.priceSL
let buyAmount = data.buyAmount
let buyPrice = data.buyPrice
let buyType = data.buyType

let top = 0//120
let bottom = 0//120

var item1st = false;
var item2nd = false;
var item3rd = false;
var item4th = false;
var item5th = false;
var item6th = false;

var lockItemsTime = [];

let level1m = 0;
let level2m = 0;
let level3m = 0;
let level4m = 0;
let level5m = 0;


async function mainHandle(){
  let isInit = await initItems();
  if(isInit){
    await checkTrade();
  }
}

async function initTopBot(){
  let purePrices = await binance.fetchOHLCV(config.pairs, '1m', undefined, 120);

  const hlPrices = purePrices.reduce((acc, price)  => {
    acc.push(price[2])
    acc.push(price[3])
    return acc
  }, [])
  var min = Math.min.apply(null, hlPrices)
  var max = Math.max.apply(null, hlPrices)
  top = max
  bottom = min

  return hlPrices

}
async function initItems(){
  let purePrices = await binance.fetchOHLCV(config.pairs, '1m', undefined, 6);
  fPrices = await formatPrices(purePrices)

  item1st = fPrices[5];
  item2nd = fPrices[4];
  item3rd = fPrices[3];
  item4th = fPrices[2];
  item5th = fPrices[1];
  item6th = fPrices[0];

  itemTop = false
  itemBot = false


  //B handle set data
  data.currentPrice = item1st.close

  
  //set top/bot
  if(data.currentPrice > top ){
    top = data.currentPrice
    itemTop = item1st
  }
  if(data.currentPrice < bottom ){
    bottom = data.currentPrice
    itemBot = item1st
  }

  data.currentChart = "normal"
  if(data.progress == "selling"){

    //set highPrice, lowPrice affter buy
    if(data.currentPrice > data.highPrice ){
      data.highPrice = data.currentPrice
    }
    if(data.currentPrice < data.lowPrice ){
      data.lowPrice = data.currentPrice
    }

    //set currentChart
    if(data.currentPrice  > data.buyPrice ){
      data.currentChart = "up"
    }else if(data.currentPrice  < data.buyPrice){
      data.currentChart = "down"
    }
  }
  //E handle set data

  // item1st.status = checkUpDown(item1st, item2nd)
  // item2nd.status = checkUpDown(item2nd, item3rd)
  // item3rd.status = checkUpDown(item3rd, item4th)
  // item4th.status = checkUpDown(item4th, item5th)
  // item5th.status = checkUpDown(item5th, item6th)

  let prices = []
  prices[0]= item1st
  prices[1]= item2nd
  level1m = await analysisAccelerate(prices);

  prices = []
  prices[0]= item1st
  prices[1]= item3rd
  level2m = await analysisAccelerate(prices);

  prices = []
  prices[0]= item1st
  prices[1]= item4th
  level3m = await analysisAccelerate(prices);

  prices = []
  prices[0]= item1st
  prices[1]= item5th
  level4m = await analysisAccelerate(prices);

  prices = []
  prices[0]= item1st
  prices[1]= item6th
  level5m = await analysisAccelerate(prices);
  console.log('level 0 >>>>', item1st)
  console.log('level 1 >>>>', level1m, item2nd)
  console.log('level 2 >>>>', level2m, item3rd)
  console.log('level 3 >>>>', level3m, item4th)
  console.log('level 4 >>>>', level4m, item5th)
  console.log('level 5 >>>>', level5m, item6th)

  return true
}

function checkUpDown(first, second){

  var status = "normal"
  if(first.close > second.close){
    status = "up"
  }else if(first.close < second.close){
    status = "down"
  }
  return status
}

async function formatPrices(prices){
  const bPrices = prices.map(price => {
    return {
      time: price[0],
      timestamp: moment(price[0]).format(),
      open: price[1],
      high: price[2],
      low: price[3],
      close: price[4],
      volume: price[5],
      status: price[1] < price[4] ? "up" : (price[1] > price[4] ? "down" : "normal"),
      change: (price[4] - price[1])*100/price[1],
      applitude: (price[2] - price[3])*100/price[3],
      lockItem: lockItemsTime.includes(moment(price[0]).format()) ? 1 : 0
    }
  })

  return bPrices
}
async function checkTrade(){

  //buying/selling
  var bottomStart = (top - bottom)/ 100 * 5 + bottom
  var bottomEnd = (top - bottom)/ 100 * 10 + bottom

  var topStart = (top - bottom)/ 100 * 95 + bottom
  var topEnd = (top - bottom)/ 100 * 90 + bottom
  console.log(" ---------- ")
  console.log(">>>>>> bottomEnd: ", bottomEnd)
  console.log(">>>>>> bottomStart: ", bottomStart)
  console.log(">>>>>> bottom: ", bottom)
  

  console.log(" ---------- ")
  console.log(">>>>>> top: ", top)
  console.log(">>>>>> topStart: ", topStart)
  console.log(">>>>>> topEnd: ", topEnd)
  console.log(" ---------- ")
  if(data.progress == "buying"){
    //item2nd.change > 0.4 bị dư, phòng trường hợp call api chậm, nếu fetch realtime thì bỏ qua
    if(item1st.lockItem == 0 && item2nd.lockItem == 0 && item1st.status == "up" && (item2nd.change > 0.4 || item1st.change > 0.4)  ){
      //trường hợp tăng đột xuất, mua đu đỉnh
      //https://drive.google.com/file/d/11uQkq69c7Q8_RNeRE3OuE6mYFyBrrvvo/view?usp=sharing
      //https://drive.google.com/drive/folders/15RN3WceEFvMVZ3m90Ne3f4N1noDegfkz?ths=true
      console.log(">>>>>> marketBuy")
      logTrade.log(">>>>> data ",data)
      order = await marketBuy(item1st.close)
      logTrade.log(">>>>> order",order)
      logTrade.log(">>>>> item1st",item1st)
      printBalance(item1st.close)
      data.buyType = "du_dinh"
      if(item1st.volume > (item2nd.volume * 2) ){
        data.buyType = "du_dinh_volume"
      }
      updateBuyData(order)
    //}else if(item2nd.status == "up" && (item3rd.change < -0.4 || item4th.change < -0.4 || item5th.change < -0.4 ) && (item3rd.status == "down" || item4th.status == "down" || item5th.status == "down") && (bottomStart < data.currentPrice  && data.currentPrice < bottomEnd) ){
    }else if(item1st.lockItem == 0 && item2nd.lockItem == 0 && item2nd.status == "up" && (item3rd.change < -0.4 || item4th.change < -0.4 || item5th.change < -0.4 ) && (item3rd.status == "down" || item4th.status == "down" || item5th.status == "down") && (bottomStart < data.currentPrice) ){
      //item thu 2 có dấu hiệu tăng, item 3/4/5 giảm mạnh và current phải chạm đáy trong khoảng 90-95% -> trường hợp giảm liên tục, dò đáy
      //https://drive.google.com/file/d/1wtRLMCWd5va8YR5CdlCv-gjLVTxXO3R7/view?usp=sharing
      //https://drive.google.com/file/d/1IvNFPNgg7BjUxbqN5BiRQ02ZglIvXMQD/view?usp=sharing
      console.log(">>>>>> marketBuy")
      logTrade.log(">>>>> data ",data)
      order = await marketBuy(item1st.close)
      logTrade.log(">>>>> order",order)
      logTrade.log(">>>>> item1st",item1st)
      printBalance(item1st.close)
      data.buyType = "do_day"
      updateBuyData(order)
    }else if(item1st.lockItem == 0 && item2nd.lockItem == 0 && item2nd.status == "up" && (item3rd.change < -0.4 || item4th.change < -0.4 || item5th.change < -0.4 ) && (item3rd.status == "down" || item4th.status == "down" || item5th.status == "down") ){
      //item thu 2 có dấu hiệu tăng, item 3/4/5 giảm mạnh
      //https://drive.google.com/file/d/1IvNFPNgg7BjUxbqN5BiRQ02ZglIvXMQD/view?usp=sharing
      console.log(">>>>>> marketBuy")
      logTrade.log(">>>>> data ",data)
      order = await marketBuy(item1st.close)
      logTrade.log(">>>>> order",order)
      logTrade.log(">>>>> item1st",item1st)
      printBalance(item1st.close)
      data.buyType = "ko_do_day"
      updateBuyData(order)
    }
  }else{//selling
    var dif = data.currentPrice - data.buyPrice
    var difPc =  dif*100/data.buyPrice
    console.log(">>>>>>>>> difPc", difPc)
    if(difPc >= config.tpMin){//2
      //TP
      //https://drive.google.com/file/d/1W0QWJQiGkfa6-S7fT7Dzkwfg-HhGqtsp/view?usp=sharing
      //https://drive.google.com/drive/folders/1n6qP25XFSPOTVvSLWn_aKIocDCpQHYx_?ths=true
      if(data.currentChart == "up"){//TP
        if(item1st.change > 1 || item2nd.change > 1 ){
          //fast keep
          if( data.currentPrice < topStart ){//95%
            console.log(">>>>>> marketSell")
            logTrade.log(">>>>> data ",data)
            order = await marketSell()
            logTrade.log(">>>>> order",order)
            logTrade.log(">>>>> item1st",item1st)
            logTrade.log("TP difPc: ",difPc)
            printBalance(item1st.close) 
            updateSellData()
          }else{
            //on top, keep it
          }
        }else{
          console.log(">>>>>> marketSell")
          logTrade.log(">>>>> data ",data)
          order = await marketSell()
          logTrade.log(">>>>> order",order)
          logTrade.log(">>>>> item1st",item1st)
          logTrade.log("TP difPc: ",difPc)
          printBalance(item1st.close) 
          updateSellData()
        }
      }else{
        console.log(">>>>>> marketSell")
        logTrade.log(">>>>> data ",data)
        order = await marketSell()
        logTrade.log(">>>>> order",order)
        logTrade.log(">>>>> item1st",item1st)
        logTrade.log("TP difPc: ",difPc)
        printBalance(item1st.close) 
        updateSellData()
      }
      
    }else if(difPc <= config.stopLoss){//-2
      //SL
      console.log(">>>>>> marketSell")
      logTrade.log(">>>>> data ",data)
      order = await marketSell()
      logTrade.log(">>>>> order",order)
      logTrade.log(">>>>> item1st",item1st)
      logTrade.log("stopLoss difPc: ",difPc)
      printBalance(item1st.close) 
      updateSellData()
    }
   
  }

  console.log(">>>>>>> data", data)
  console.log(">>>>>>> config", config)
  
}

const { Console } = require("console");
const logTrade = new Console({
  stdout: fs.createWriteStream(appDir + "/log/trade.log", {flags : 'a'}),
});

async function updateBuyData(order){

  callBack = function(err) {
    if (err) throw err;
    console.log('updateBuyData callBack completed');
  }
  
  data.buyAmount = order.amount
  data.buyPrice = order.price
  data.progress = "selling"

  fs.writeFile(appDir + "/data.json", JSON.stringify(data), 'utf8', callBack);
  console.log(">>>> updateBuyData")

}

async function updateSellData(){

  callBack = function(err) {
    if (err) throw err;
    console.log('updateSellData callBack completed');
  }

  data.buyAmount = 0
  data.progress = "buying"
  data.buyPrice = 0
  data.highPrice = 0
  data.lowPrice = 0
  data.buyType = ""

  fs.writeFile(appDir + "/data.json", JSON.stringify(data), 'utf8', callBack);
  console.log(">>>> updateSellData")

}

async function printBalance(btcPrice){
  const balance = await binance.fetchBalance();
  const total = balance.total
  const msg1 = `Balance: BTC ${total.BTC}, USDT: ${total.USDT}`
  data.totalUSDT = (total.BTC - 1) * btcPrice + total.USDT
  const msg2 = `Total USDT: ${data.totalUSDT}. \n`
  console.log(msg1);
  console.log(msg2);

  logTrade.log(msg1)
  logTrade.log(msg2)
  logTrade.log("-----------")

}
async function marketBuy(lastPrice){
 
  const quantity = TRADE_SIZE / lastPrice
  const order = await binance.createMarketOrder(config.pairs, 'buy', quantity)
  return order
}

async function marketSell(){
 
  const quantity = data.buyAmount
  const order = await binance.createMarketOrder(config.pairs, 'sell', quantity)
  //B lock item
  lockItemsTime = []
  lockItemsTime.push(item1st.timestamp, item2nd.timestamp, item3rd.timestamp)
  //E lock item
  return order
}
async function analysisAccelerate(prices){
  
  var hlPrices = await OHLCVformatHightLow(prices)

  var min = Math.min.apply(null, hlPrices)
  var max = Math.max.apply(null, hlPrices)

  let dif = max - min;

  let level = 0
  if(dif > 0){
    level = (dif*100)/min
  }else if(dif < 0){
    level = (dif*100)/max
  }
  return level
}


async function OHLCVformatHightLow(prices){

  const hlPrices = prices.reduce((acc, price)  => {
    acc.push(price.high)
    acc.push(price.low)
    return acc
  }, [])
  return hlPrices
}
async function loopLoop(){
  while(true){
    await mainHandle();
    //await dalay(60* 1000);//60s
    await dalay(30* 1000);//20s
  }
}

async function loopSolf(){
  //update gia tốc, currentPrice item1st mỗi 5s, để đảm bảo trong trường hợp giá tăng giảm mạnh có thể control
  //if != time tức là đã có nến mới, call mainHandle() if lastcall > 10s
}

//node btc/case1m.js

initTopBot()
//mainHandle()
loopLoop()
//await dalay(5* 1000);




