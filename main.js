const config = require("./config.json");
const { dirname } = require("path");
const moment = require("moment");
const appDir = dirname(require.main.filename);

function formatMA(prices, ma) {
  if (ma != 7) {
    ma = 3;
  }
  for (let i = 0; i < 7; i++) {
    let bPrices = prices.slice(i, i + ma);
    const averagePrice =
      bPrices.reduce((acc, price) => acc + price.close, 0) / ma;
    if (ma == 7) {
      prices[i]["ma7"] = averagePrice;
    } else {
      prices[i]["ma3"] = averagePrice;
    }
  }
  return prices;
}

function handleFormatMA(prices) {
  let format = formatMA(prices, 7);
  format = formatMA(prices, 3);
  return format;
}
function formatPrices(prices, lockItemsTime) {
  const bPrices = prices.map((price) => {
    let tmpFormat = {
      time: price[0],
      timestamp: moment(price[0]).format(),
      open: price[1],
      high: price[2],
      low: price[3],
      close: price[4],
      volume: price[5],
      volumeTo: price[4] * price[5],
      status:
        price[1] < price[4] ? "up" : price[1] > price[4] ? "down" : "normal",
      change: ((price[4] - price[1]) * 100) / price[1],
      applitude: ((price[2] - price[3]) * 100) / price[3],
      lockItem: lockItemsTime.includes(moment(price[0]).format()) ? 1 : 0,
      ma7: 0,
      ma7OrderStt: "normal",
      accelerate: 0, //init
      ma3: 0,
      ma3OrderStt: "normal",
    };

    return tmpFormat;
  });
  const reversed = bPrices.reverse();
  let pricesFormat = handleFormatMA(reversed);
  return pricesFormat;
}

module.exports = {
  privateConfig: config,
  config: config,
  appDir: appDir,
  formatMA: formatMA,
  handleFormatMA: handleFormatMA,
  formatPrices: formatPrices,
};
