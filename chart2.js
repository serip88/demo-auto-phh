const chart = require('asciichart')
const fs = require('fs')
const dalay = require('delay');

//get data từ a_log.txt

function plotAsset(){
  const lines = fs.readFileSync('./log/a_log.txt', 'utf8').split('\n')
  const assets = []

  // for (const line of lines){
  //   const asset = line.trim()
  //   if(asset){
  //     assets.push(parseFloat(asset))
  //   }
    
  // }
  const start = lines.length - 192
  lines.forEach( (line, key) => {
    if( key > start){
      const asset = line.trim()
      if(asset){
        assets.push(parseFloat(asset))
      }
    }
  });

  console.clear()
  //console.log(assets)
  console.log(chart.plot(assets, {
    height: 40
  }))
}

//node chart2.js
async function loopLoop(){
  while(true){
    await plotAsset();
    await dalay(10* 1000);
  }
}
loopLoop()
//plotAsset()

