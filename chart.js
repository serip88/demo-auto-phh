const chart = require('asciichart')
const fs = require('fs')
const dalay = require('delay');

function plotAsset(){
  const lines = fs.readFileSync('./trade_log.txt', 'utf8').split('\n')
  const assets = []

  for (const line of lines){
    if(line.includes('Total USDT')){
      const asset = line.replace('Total USDT:', '').trim()
      assets.push(parseFloat(asset))
    }
  }

  console.clear()
  console.log(chart.plot(assets, {
    height: 30
  }))
}

//plotAsset()
//node chart.js
async function loopLoop(){
  while(true){
    await plotAsset();
    await dalay(10* 1000);
  }
}
loopLoop()