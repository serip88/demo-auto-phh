const dalay = require("delay");
const folder = "candleV3";
const config = require("../config/" + folder + "/setting.json");
let data = require("../config/" + folder + "/data.json");
let pairs = config.coin.toUpperCase() + "/" + config.coinTo.toUpperCase(); //TRX/USDT
const got = require("got");
const jsdom = require("jsdom");
const { JSDOM } = jsdom;

let { appDir, privateConfig, formatPrices } = require("../main");
const fs = require("fs");
const ccxt = require("ccxt");
let apiSec = {
  apiKey: privateConfig.API_R,
  secret: privateConfig.SECRET_R,
};
if (config.real === true) {
  console.log(">>>>>>>>>>>> config.real", config.real);
  apiSec.apiKey = privateConfig.API_SM;
  apiSec.secret = privateConfig.SECRET_SM;
}
const binance = new ccxt.binance(apiSec);
//binance.setSandboxMode(true);

//coin = tct
//timeStr = "1m" to fetch
//timeStr = "4h" to get min max graph
async function getLastPrice(timeStr, jump, format) {
  pairs = data.coin.toUpperCase() + "/" + config.coinTo.toUpperCase();
  if (timeStr === false) {
    timeStr = "1m";
  }
  if (jump === false) {
    jump = 7;
  }
  if (format === true) {
    jump = 7 + 6;
  }
  const prices = await binance.fetchOHLCV(pairs, timeStr, undefined, jump);
  if (format === true) {
    const bPrices = formatPrices(prices, lockItemsTime);

    return bPrices;
  } else {
    return prices;
  }
}

function chkRedGreenVol() {
  let checkRedGreen = false;
  if (item1st.volumeTo > newestRed.volumeTo) {
    checkRedGreen = true;
  } else if (item1st.volumeTo > newestGreen.volumeTo) {
    //tránh tình trạng mua trễ, tp ko kịp
    //trường hợp đồ thị giảm, có dấu hiệu tăng nhẹ sẽ vô case này
    //=> khả năng tp ít
    //trường hợp đồ thị tăng mạnh thì trường hợp nào cũng dính
    checkRedGreen = true;
  }

  return checkRedGreen;
}
function setNewestGreenRed(fPrices) {
  newestRed = false;
  newestGreen = false;
  fPrices.forEach((item, index) => {
    if (index > 0) {
      if (item.status == "up") {
        if (newestGreen == false) {
          newestGreen = item;
        }
      } else if (item.status == "down") {
        if (newestRed == false) {
          newestRed = item;
        }
      }
    }
  });
}
let item1st = false;
let item2nd = false;
let item3rd = false;
let item4th = false;
let item5th = false;
let item6th = false;

let newestRed = false;
let newestGreen = false;

let lockItemsTime = [];

let level1m = 0;
let level2m = 0;
let level3m = 0;
let level4m = 0;
let level5m = 0;
let level6m = 0;

//======= test param
let stopBot = false;
let startItem = 0;
data.currentCheckKey = 0;

async function checkDataSet() {
  if (data.progress == "buying") {
    fs.readFile(
      "./config/" + folder + "/data.json",
      "utf8",
      function (err, dataEncode) {
        let dataDecode = JSON.parse(dataEncode);
        // if (data.hardBuy != dataDecode.hardBuy) {
        //   data.hardBuy = dataDecode.hardBuy;
        // }
        // if (data.coin != dataDecode.coin) {
        //   data.coin = dataDecode.coin;
        // }
        if (data.waitHardBuy != dataDecode.waitHardBuy) {
          data.waitHardBuy = dataDecode.waitHardBuy;
        }
        if (data.tpMin != dataDecode.tpMin) {
          //set from trigger, first time get from config.tpMin
          data.tpMin = dataDecode.tpMin;
        }
        if (data.stopAfterSell != dataDecode.stopAfterSell) {
          data.stopAfterSell = dataDecode.stopAfterSell;
        }
        if (data.hardBuy_cmt != dataDecode.hardBuy_cmt) {
          data.hardBuy_cmt = dataDecode.hardBuy_cmt;
        }
        if (dataDecode.stopBot == true) {
          data.stopBot = true;
        }
        if (data.timeLossTmp > 0 && data.tpQuick != dataDecode.tpQuick) {
          data.tpQuick = dataDecode.tpQuick;
        }
        // Display the file content
        //console.log(">>>>>>> data.hardBuy_cmt", data);
      }
    );
  } else {
    //selling
    fs.readFile(
      "./config/" + folder + "/data.json",
      "utf8",
      function (err, dataEncode) {
        let dataDecode = JSON.parse(dataEncode);
        if (dataDecode.stopBot == true) {
          data.stopBot = true;
        }
        if (dataDecode.hardSell == true) {
          data.hardSell = true;
        }
        if (data.stopAfterSell != dataDecode.stopAfterSell) {
          data.stopAfterSell = dataDecode.stopAfterSell;
        }
        if (data.tpQuick != dataDecode.tpQuick) {
          data.tpQuick = dataDecode.tpQuick;
        }
        if (data.tpMin != dataDecode.tpMin) {
          //set from trigger, first time get from config.tpMin
          data.tpMin = dataDecode.tpMin;
        }
      }
    );
  }
}
async function mainHandle() {
  data.commentB_debug = [];
  data.comment_debug = [];
  //B update realtime params: hardBuy if data == buying

  if (data.stopBot == true) {
    return;
  }
  if (data.waitHardBuy == true && data.hardBuy == false) {
    console.log(">>>>>>> startItem", startItem);
    console.log(">>>>>>> waitHardBuy hardBuy", data.hardBuy);
    //after sell will wait for hardBuy, sell one time
    return;
  }
  //E update realtime params: hardBuy if data == buying
  pairs = data.coin.toUpperCase() + "/" + config.coinTo.toUpperCase();
  let isInit = await initItems();
  if (isInit) {
    await checkTrade();
  }
}

function checkStopBot() {
  let status = false;
  if (config.slContinuousOn) {
    if (data.slContinuous <= 0) {
      status = true;
    } else {
      status = false;
    }
  } else {
    status = false;
  }
  if (data.stopBot == true) {
    status = true;
  }
  if (status == true) {
    data.stopBot = false;
    saveData();
  }
  return status;
}
function reduceSlContinuousTmp() {
  //reduce slContinuous after SL
  let allowSell = false;
  data.slContinuousTmp = data.slContinuousTmp - 1;
  const timeLossTmp = config.slContinuousTmp - data.slContinuousTmp;
  if (data.slContinuousTmp > 0) {
    data.progress = "buying";
    //data.hardBuy = true; consider about this
  } else {
    allowSell = true;
  }
  data.timeLossTmp = timeLossTmp;
  return allowSell;
}
function reduceSlContinuous() {
  //reduce slContinuous after SL
  data.slContinuous = data.slContinuous - 1;
  if (data.slContinuous <= 0 && config.buyMode == "trigger") {
    data.maxTriggerLoss = data.maxTriggerLoss - 1;
    if (data.maxTriggerLoss > 0) {
      data.waitHardBuy = true;
      initSlContinuous(); //reset slContinuous
    }
  }
}

function initSlContinuous() {
  //init slContinuous for first time or affter TP
  data.slContinuous = config.slContinuous;
  data.slContinuousTmp = config.slContinuousTmp;
  data.timeLossTmp = 0;
}

async function initTotalAmount(item) {
  if (data.progress == "buying") {
    const balance = await binance.fetchBalance();
    let coin = data.coin.toUpperCase();
    data.balance.VCOIN = balance.total[coin];
    data.balance.USDT = balance.total["USDT"];
    var currentPrice = item[4]; //close
    data.totalUSDT = data.balance.VCOIN * currentPrice + data.balance.USDT;
    //cần update totalUSDT vì stopBotAmountUp, stopBotAmountDown set onlyCheck dựa vào totalUSDT để mua
  }
}
async function initTopBot() {
  //run first time
  data.items = [];
  if (config.checkMa7BuyUp) {
    data.flagMa7BuyUp = true;
  }
  //importance cause it reset slContinuous/slContinuousTmp on progress
  if (data.progress == "buying" && data.timeLossTmp == 0) {
    initSlContinuous();
  }
  //let purePrices = dataLogPure;
  //108 = 1 man hinh mac 13
  if (data.progress == "selling" && data.waitHardBuy == true) {
    //keep data.tpMin
  } else {
    data.tpMin = config.tpMin; //set tpMin
  }

  let purePrices = await getLastPrice(
    config.timeframeBotTop,
    config.fetchBotTopLimit,
    false
  );
  //console.log(">>>>purePrices", purePrices);
  const hlPrices = purePrices.reduce((acc, price) => {
    acc.push(price[2]);
    acc.push(price[3]);
    return acc;
  }, []);
  let min = Math.min.apply(null, hlPrices);
  let max = Math.max.apply(null, hlPrices);
  data.chartTop = max;
  data.chartBottom = min;

  console.log(">>>>min max", min, max);

  await initTotalAmount(purePrices[0]);
  initCheckBuy();
  return hlPrices;
}
function checkTpSlDay() {
  let d1 = new Date();
  console.log(d1.toISOString().split("T")[0]);
  let d2 = new Date(d1);
  d2.setDate(d2.getDate() + 1);
  let tpSlDay = {};
  tpSlDay.tpDay = 1;
  tpSlDay.slDay = 2;
  tpSlDay.startToday = data.totalUSDT;
  tpSlDay.pot = config.TRADE_SIZE * 3;
  tpSlDay.potTP = (tpSlDay.pot / 100) * (100 + tpSlDay.tpDay);
  tpSlDay.potSL = (tpSlDay.pot / 100) * (100 - tpSlDay.slDay);
  tpSlDay.tpDayAmount = tpSlDay.startToday - tpSlDay.pot + tpSlDay.potTP;
  tpSlDay.slDayAmount = tpSlDay.startToday - tpSlDay.pot + tpSlDay.potSL;
  tpSlDay.today = d1.toISOString().split("T")[0];
  tpSlDay.nextDay = d2.toISOString().split("T")[0];
  if (data.tpSlDay.today != tpSlDay.today) {
    data.tpSlDay = tpSlDay;
  }

  return tpSlDay;
}
function initCheckBuy() {
  return data.checkBuy;
}

function checkBuy(fPrices) {
  return data.checkBuy;
}

async function initItems() {
  let timeframeRun = config.timeframeRun;
  let limitRange = 7;
  if (data.progress == "checking") {
    limitRange += 6;
    timeframeRun = "1m";
  }
  fPrices = await getLastPrice(timeframeRun, limitRange, true);
  checkBuy(fPrices);
  setNewestGreenRed(fPrices);

  item1st = fPrices[0];
  item2nd = fPrices[1];
  item3rd = fPrices[2];
  item4th = fPrices[3];
  item5th = fPrices[4];
  item6th = fPrices[5];
  item7th = fPrices[6];

  itemTop = false;
  itemBot = false;

  //B handle set data
  data.currentPrice = item1st.close;

  //B handle ma7
  data.ma7 = item1st.ma7;
  if (data.currentPrice > data.ma7) {
    data.ma7OrderStt = "up";
  } else if (data.currentPrice < data.ma7) {
    data.ma7OrderStt = "down";
  } else {
    data.ma7OrderStt = "normal";
  }
  //E handle ma7

  //set data.chartTop/bot
  if (data.currentPrice > data.chartTop) {
    data.chartTop = data.currentPrice;
    itemTop = item1st;
  }
  if (data.currentPrice < data.chartBottom) {
    data.chartBottom = data.currentPrice;
    itemBot = item1st;
  }

  data.currentChart = "normal";
  if (data.progress == "selling") {
    //set affterBuyHighPrice, affterBuyLowPrice affter buy
    /*if (data.currentPrice > data.affterBuyHighPrice) {
      data.affterBuyHighPrice = data.currentPrice
    }
    if (data.currentPrice < data.affterBuyLowPrice) {
      data.affterBuyLowPrice = data.currentPrice
    }*/
    if (item1st.high > data.affterBuyHighPrice) {
      data.affterBuyHighPrice = item1st.high;
    }
    if (item1st.low < data.affterBuyLowPrice) {
      data.affterBuyLowPrice = item1st.low;
    }

    // let aUnit = (data.affterBuyHighPrice - data.affterBuyLowPrice) / 100
    // data.currentPercentAffterBuy = (data.currentPrice - data.affterBuyLowPrice) / aUnit
    data.currentPercentAffterBuy = 0;
    if (data.currentPrice > data.buyPrice) {
      let aUnit = (data.affterBuyHighPrice - data.buyPrice) / 100;
      data.currentPercentAffterBuy =
        (data.currentPrice - data.buyPrice) / aUnit;
    } else if (data.currentPrice < data.buyPrice) {
      data.currentPercentAffterBuy =
        (data.currentPrice * 100) / data.buyPrice - 100;
    }
    //set currentChart
    if (data.currentPrice > data.buyPrice) {
      data.currentChart = "up";
    } else if (data.currentPrice < data.buyPrice) {
      data.currentChart = "down";
    }
  }
  //E handle set data

  // item1st.status = checkUpDown(item1st, item2nd)
  // item2nd.status = checkUpDown(item2nd, item3rd)
  // item3rd.status = checkUpDown(item3rd, item4th)
  // item4th.status = checkUpDown(item4th, item5th)
  // item5th.status = checkUpDown(item5th, item6th)

  let prices = [];
  prices[0] = item1st;
  prices[1] = item2nd;
  level1m = analysisAccelerate(prices);
  item1st.accelerate = level1m;
  handlePushItems(item1st);

  prices = [];
  prices[0] = item1st;
  prices[1] = item3rd;
  level2m = analysisAccelerate(prices);
  item2nd.accelerate = level2m;

  prices = [];
  prices[0] = item1st;
  prices[1] = item4th;
  level3m = analysisAccelerate(prices);
  item3rd.accelerate = level3m;

  prices = [];
  prices[0] = item1st;
  prices[1] = item5th;
  level4m = analysisAccelerate(prices);
  item4th.accelerate = level4m;

  prices = [];
  prices[0] = item1st;
  prices[1] = item6th;
  level5m = analysisAccelerate(prices);
  item5th.accelerate = level5m;

  prices = [];
  prices[0] = item1st;
  prices[1] = item7th;
  level6m = analysisAccelerate(prices);
  item6th.accelerate = level6m;

  if (data.progress != "checking") {
    /*console.log("level 0 >>>>", item1st);
    console.log("level 1 >>>>", level1m, item2nd);
    console.log("level 2 >>>>", level2m, item3rd);
    console.log("level 3 >>>>", level3m, item4th);
    console.log("level 4 >>>>", level4m, item5th);
    console.log("level 5 >>>>", level5m, item6th);
    console.log("level 6 >>>>", level6m, item7th);

    console.log("newestRed >>>>", newestRed);
    console.log("newestGreen >>>>", newestGreen);*/
  }

  let accelerates = [];
  let tmpAcc = 0;

  tmpAcc =
    item1st.status == "down" ? item1st.accelerate * -1 : item1st.accelerate;
  accelerates.push(tmpAcc);
  tmpAcc =
    item2nd.status == "down" ? item2nd.accelerate * -1 : item2nd.accelerate;
  accelerates.push(tmpAcc);
  tmpAcc =
    item3rd.status == "down" ? item3rd.accelerate * -1 : item3rd.accelerate;
  accelerates.push(tmpAcc);
  tmpAcc =
    item4th.status == "down" ? item4th.accelerate * -1 : item4th.accelerate;
  accelerates.push(tmpAcc);
  tmpAcc =
    item5th.status == "down" ? item5th.accelerate * -1 : item5th.accelerate;
  accelerates.push(tmpAcc);
  tmpAcc =
    item6th.status == "down" ? item6th.accelerate * -1 : item6th.accelerate;
  accelerates.push(tmpAcc);

  data.accelerates = accelerates.join(", ");
  return true;
}

function handlePushItems(currentItem) {
  var today = new Date();
  let locDate = today.toLocaleDateString();
  let timeString = today.toTimeString().split(" ")[0];
  let locDateString = [locDate, timeString].join(" ");
  let item = {
    time: locDateString,
    status: "same",
    dif: 0,
    price: currentItem.close,
    volume: currentItem.volume,
    volumeTo: currentItem.volumeTo,
  };
  if (data.items.length > 0) {
    let lastIndex = data.items.length - 1;
    let lastItemEn = data.items[lastIndex];
    let lastItem = JSON.parse(lastItemEn);
    item.dif = item.price - lastItem.price;
    if (lastItem.price < item.price) {
      item.status = "up";
    } else if (lastItem.price > item.price) {
      item.status = "down";
    }
  }
  //data.items.push(item);
  data.items.push(JSON.stringify(item));
  if (data.items.length > 10) {
    data.items.shift();
  }
}

function checkUpDown(first, second) {
  let status = "normal";
  if (first.close > second.close) {
    status = "up";
  } else if (first.close < second.close) {
    status = "down";
  }
  return status;
}

function getNextPercentTP(tpMin, timeLoss) {
  //tpMin=2
  //timeLoss: 0~3
  //tpMinNew: 2,3,4,5
  let tpMinNew = (tpMin / 2) * timeLoss + tpMin;
  return tpMinNew;
}
function checkVolChange() {
  let stt = false;
  if (item1st.status == "up" && item2nd.status == "up") {
    if (item1st.volume > item2nd.volume * 2) {
      stt = true;
    }
  } else if (item1st.status == "up" && item2nd.status == "down") {
    if (item1st.volume > item2nd.volume * 2) {
      stt = true;
    }
  }
  return stt;
}

function checkAllowBuy() {
  if (item1st.lockItem == 0) {
    return true;
  } else {
    return false;
  }
}
function checkAllowBuy_bk() {
  let timeAllowBuy = new Date(data.newestActionTime);
  timeAllowBuy.setMinutes(timeAllowBuy.getMinutes() + 10);

  let current = new Date();
  if (current.getTime() > timeAllowBuy.getTime()) {
    return true;
  } else {
    return false;
  }
}
function checkStopAmountCommon() {
  if (data.totalUSDT > data.stopBotAmountUp) {
    config.onlyCheck = true;
  } else if (data.totalUSDT < data.stopBotAmountDown) {
    config.onlyCheck = true;
  }
  if (config.onlyCheck == true && data.progress == "selling") {
    config.onlyCheck = false;
  }
}
function checkStopAmountDay() {
  // let stopBotAmountUp = data.stopBotAmountUp;
  // let stopBotAmountDown = data.stopBotAmountDown;
  let stopBotAmountUp = data.tpSlDay.tpDayAmount;
  let stopBotAmountDown = data.tpSlDay.slDayAmount;
  if (data.totalUSDT > stopBotAmountUp) {
    config.onlyCheck = true;
  } else if (data.totalUSDT < stopBotAmountDown) {
    config.onlyCheck = true;
  } else {
    //config.onlyCheck = false;
  }
  if (config.onlyCheck == false) {
    checkStopAmountCommon();
  }
}
async function checkTrade() {
  //buying/selling
  data.chart5 =
    ((data.chartTop - data.chartBottom) / 100) * 5 + data.chartBottom;
  //chart5 = chartBottom + 5%
  data.chart10 =
    ((data.chartTop - data.chartBottom) / 100) * 10 + data.chartBottom;
  //chart10 = chartBottom + 10%
  data.chart95 =
    ((data.chartTop - data.chartBottom) / 100) * 95 + data.chartBottom;
  data.chart90 =
    ((data.chartTop - data.chartBottom) / 100) * 90 + data.chartBottom;
  console.log(" ---------- ");
  console.log(">>>>>> data.chart10: ", data.chart10);
  console.log(">>>>>> data.chart5: ", data.chart5);
  console.log(">>>>>> data.chartBottom: ", data.chartBottom);

  console.log(" ---------- ");
  console.log(">>>>>> data.chartTop: ", data.chartTop);
  console.log(">>>>>> data.chart95: ", data.chart95);
  console.log(">>>>>> data.chart90: ", data.chart90);
  console.log(" ---------- ");
  if (config.onlyCheck == false) {
    if (data.progress == "buying") {
      let allowBuy = false;
      allowBuy = checkAllowBuy();
      console.log(">>>>>>>>> checkAllowBuy", allowBuy);

      if (data.hardBuy == true) {
        data.hardBuy = false;
        data.waitHardBuy = false;
        data.buyType = "hard_buy_coin: " + data.coin;
        console.log(">>>>>> marketBuy");
        data.comment_debug.push(["code_log", 10012]);
        order = await handleMarketBuy(item1st.close);
      } else if (allowBuy == true) {
        // allowBuy = item2nd.status == "down";
        // if (allowBuy == false) {
        //   allowBuy = item2nd.status == "up" && item3rd.status == "down";
        // } else {
        //   allowBuy = item2nd.status == "up" && item3rd.status == "up";
        // }
        if (item1st.status == "up" && allowBuy == true) {
          data.buyCandle = item2nd.high;
          if (
            (item2nd.status == "up" && item3rd.status == "down") ||
            (item2nd.status == "up" && item3rd.status == "up")
          ) {
            if (item3rd.high < item2nd.high) {
              data.buyCandle = item3rd.high;
            }
          }
          //B check newestRed, newestGreen
          let checkRedGreen = chkRedGreenVol();
          //E check newestRed, newestGreen
          if (data.currentPrice > data.buyCandle && checkRedGreen == true) {
            data.buyType = "buyCandle";
            console.log(">>>>>> marketBuy");
            data.comment_debug.push(["code_log", 10014]);
            order = await handleMarketBuy(item1st.close);
          }
        }
      }
    } else {
      //progress == selling
      let dif = data.currentPrice - data.buyPrice;
      let difPc = (dif * 100) / data.buyPrice;
      data.difPc = difPc;
      console.log(">>>>>>>>> difPc", difPc);

      let tpMin = data.tpMin;

      data.tpTimeLoss = tpMin;
      data.slPrice = data.buyPrice + (data.buyPrice / 100) * config.stopLoss; //stopLoss = -1.9 so will +
      data.tpPrice = data.buyPrice + (data.buyPrice / 100) * tpMin;
      if (data.tpQuick > 0 && tpMin > data.tpQuick) {
        tpMin = data.tpQuick; //after S, set data.tpQuick = 0
        data.usingTpQuick = true; //after S, set data.usingTpQuick = false
      }
      let tpMax = 77; //%
      let tpSafe = 1.7;
      if (data.hardSell == true) {
        data.hardSell = false;
        data.sellType = "hard_sell";
        data.comment_debug.push(["code_log", 10011]);
        order = await handleMarketSell(data, item1st);
      } else if (data.difPc > 0) {
        data.comment = "selling data.difPc > 0 >>>>>>>>";
        if (data.difPc >= tpMin) {
          //4
          data.sellType = "tp_tpMin";
          data.comment_debug.push(["code_log", 10005]);
          order = await handleMarketSell(data, item1st);
        } else if (item1st.status == "down") {
          if (data.currentPercentAffterBuy < tpMax && data.difPc > tpSafe) {
            data.comment_debug.push(["code_log", 10008]);
            order = await handleMarketSell(data, item1st);
          }
        } else if (
          data.currentPercentAffterBuy < tpMax &&
          data.difPc > tpSafe
        ) {
          data.comment_debug.push(["code_log", 10009]);
          order = await handleMarketSell(data, item1st);
        }
      } else if (difPc <= config.stopLoss) {
        data.comment = "selling difPc <= config.stopLoss >>>>>>>>";
        //stopLossTop
        //-2% => -3%
        //SL
        if (difPc > config.stopLossBottom) {
          // not sell at case reduce to much

          data.sellType = "stop_loss";
          data.comment_debug.push(["code_log", 10010]);
          order = await handleMarketSell(data, item1st);
        }
      }
    }
  }
  console.log(">>>>>>> config", config);
  console.log(">>>>>>> data", data);
  console.log(">>>>>>> startItem", startItem);
  console.log(">>>>>>> onlyCheck: ", config.onlyCheck);
}

const { Console } = require("console");
const logTrade = new Console({
  stdout: fs.createWriteStream("./config/" + folder + "/trade.log", {
    flags: "a",
  }),
});

async function saveData() {
  callBack = function (err) {
    if (err) throw err;
    console.log("saveData callBack completed");
  };
  fs.writeFile(
    "./config/" + folder + "/data.json",
    JSON.stringify(data),
    "utf8",
    callBack
  );
}
async function updateBuyData() {
  data.progress = "selling";
  data.limitBuyPrice = 0;
  data.limitBuyCoin = data.coin;
  data.newestActionTime = item1st.timestamp;

  saveData();
  initCheckBuy();
  console.log(">>>> updateBuyData");
}

async function updateSellData() {
  data.buyAmount = 0;
  //data.progress = "buying";
  data.progress = "checking";
  data.buyPrice = 0;
  data.affterBuyHighPrice = 0;
  data.affterBuyLowPrice = 0;
  data.buyType = "";
  data.tpQuick = 0;
  data.usingTpQuick = false;
  data.newestActionTime = item1st.timestamp;

  if (data.difPc > 0) {
    //TP
    data.tpAccelerate = level1m;
    let limitBuyPrice = getLimitBuyPrice();
    console.log(">>>> limitBuyPrice", limitBuyPrice);
    data.limitBuyPrice = parseFloat(limitBuyPrice); //after buy remove it
    data.limitBuyCoin = data.coin; //after buy remove it
    if (config.checkMa7BuyUp == true) {
      data.flagMa7BuyUp = true;
    }
    data.countTp += 1;
  } else {
    data.countSl += 1;
  }
  if (data.stopAfterSell == true) {
    data.stopAfterSell = false;
    data.stopBot = true;
  }

  saveData();
  console.log(">>>> updateSellData");
}

async function printBalance(currentPrice) {
  const msg1 = `Balance: VCOIN: ${data.balance.VCOIN}, USDT: ${data.balance.USDT}`;
  data.totalUSDT = data.balance.VCOIN * currentPrice + data.balance.USDT;
  const msg2 = `Total USDT: ${data.totalUSDT}. \n`;
  console.log(msg1);
  console.log(msg2);

  logTrade.log(msg1);
  logTrade.log(msg2);
  logTrade.log("-----------");
}

function getQuantityBuy(lastPrice) {
  let quantity = config.TRADE_SIZE / lastPrice;
  //B check ruleNextBuy
  if (config.ruleNextBuy == "add_more") {
    let timeLoss = config.slContinuous - data.slContinuous;
    if (timeLoss > 2) {
      timeLoss = 2;
    }
    quantity = (config.TRADE_SIZE * (timeLoss + 1)) / lastPrice;
  }
  //E check ruleNextBuy
  return quantity;
}
async function marketBuy(lastPrice) {
  const quantity = getQuantityBuy(lastPrice);

  const order = await binance.createMarketOrder(pairs, "buy", quantity);
  data.affterBuyHighPrice = order.price;
  data.affterBuyLowPrice = order.price;
  data.sellType = "";
  data.buyPrice = order.price;

  const balance = await binance.fetchBalance();
  let coin = data.coin.toUpperCase();
  data.balance.VCOIN = balance.total[coin];
  data.balance.USDT = balance.total["USDT"];
  //after mua 2nd,3rd will add more
  data.buyAmount = data.balance.VCOIN;

  return order;
}
async function marketSell() {
  console.log(">>>>>> marketSell");
  const quantity = data.buyAmount;
  const order = await binance.createMarketOrder(pairs, "sell", quantity);
  //B lock item
  lockItemsTime = [];
  lockItemsTime.push(item1st.timestamp, item2nd.timestamp, item3rd.timestamp);
  //E lock item
  if (data.sellType == "stop_loss") {
    reduceSlContinuous();
  } else {
    //TP
    data.maxTriggerLoss = config.maxTriggerLoss;
    initSlContinuous();
  }
  return order;
}
function getLimitBuyPrice() {
  let prices = [];
  if (item1st.status == "up") {
    prices.push(item1st);
  }
  if (item2nd.status == "up") {
    prices.push(item2nd);
  }
  if (item3rd.status == "up") {
    prices.push(item3rd);
  }
  if (item4th.status == "up") {
    prices.push(item4th);
  }
  if (item5th.status == "up") {
    prices.push(item5th);
  }
  if (item6th.status == "up") {
    prices.push(item6th);
  }
  if (item7th.status == "up") {
    prices.push(item7th);
  }

  let hlPrices = OHLCVformatHightLow(prices);

  let min = Math.min.apply(null, hlPrices);
  let max = Math.max.apply(null, hlPrices);
  return min;
}
async function handleMarketBuy(lastPrice) {
  let order = false;
  if (config.real === true) {
    if (config.slContinuousTmpOn == true) {
      let halfQuantity = config.TRADE_SIZE / 2 / data.currentPrice;
      if (
        data.timeLossTmp > 0 &&
        data.buyAmount >= halfQuantity &&
        data.currentPrice >= data.slPrice
      ) {
        //check case increase back
        data.timeLossTmp = data.timeLossTmp - 1;
        data.slContinuousTmp = data.slContinuousTmp + 1;
        data.progress == "selling";
      } else {
        //normal buy
        order = await marketBuy(lastPrice);
      }
    } else {
      order = await marketBuy(lastPrice);
    }
  } else {
    order = await marketBuyLocal(lastPrice);
  }
  if (order) {
    logTrade.log(">>>>> data ", data);
    logTrade.log(">>>>> order", order);
    logTrade.log(">>>>> item1st", item1st);
    printBalance(item1st.close);
    updateBuyData();
  }
  return order;
}
async function marketBuyLocal(lastPrice) {
  const quantity = config.TRADE_SIZE / lastPrice;
  const order = await createMarketOrderLocal(pairs, "buy", quantity);
  return order;
}
async function createMarketOrderLocal(pairs, mode, quantity) {
  let order = {
    timestamp: item1st.timestamp,
    datetime: item1st.time,
    symbol: pairs,
    type: "market",
    timeInForce: "GTC",
    side: mode,
    price: data.currentPrice,
    amount: quantity,
    cost: data.currentPrice * quantity,
    average: 43394.61,
    filled: 0.002309,
    remaining: 0,
    status: "closed",
  };
  if (mode == "buy") {
    //nếu đủ tiền mới mua
    if (data.balance.USDT > config.TRADE_SIZE) {
      data.balance.USDT = data.balance.USDT - config.TRADE_SIZE;
      data.balance.VCOIN = data.balance.VCOIN + order.amount;
      data.affterBuyHighPrice = order.price;
      data.affterBuyLowPrice = order.price;
      data.sellType = "";
      data.buyAmount = order.amount;
      data.buyPrice = order.price;
    } else {
      logTrade.log("-----------");
      logTrade.log("Not enough USDT");
      return false;
    }
  } else {
    data.balance.USDT = data.balance.USDT + order.cost;
    data.balance.VCOIN = data.balance.VCOIN - order.amount;
    data.buyType = "";
  }

  return order;
}

async function handleMarketSell(data, item1st) {
  console.log(">>>>> handleMarketSell", data);
  let allowSell = true;
  if (config.slContinuousTmpOn == true && data.sellType == "stop_loss") {
    allowSell = reduceSlContinuousTmp();
  }

  let order = false;
  if (allowSell == true) {
    if (config.real == true) {
      order = await marketSell();
    } else {
      order = await marketSellLocal();
    }
    data.slContinuousTmp = config.slContinuousTmp;
    data.timeLossTmp = 0;

    logTrade.log(">>>>> data ", data);
    logTrade.log(">>>>> order", order);
    logTrade.log(">>>>> item1st", item1st);
    if (data.difPc > 0) {
      logTrade.log("TP difPc: ", data.difPc);
    } else {
      logTrade.log("stopLoss difPc: ", data.difPc);
    }
    printBalance(item1st.close);
    updateSellData(); //save data too
  } else {
    //allow to buy one more
    saveData();
  }

  return order;
}
async function marketSellLocal() {
  console.log(">>>>>> marketSellLocal");
  const quantity = data.buyAmount;
  const order = await createMarketOrderLocal(pairs, "sell", quantity);
  //B lock item
  lockItemsTime = [];
  lockItemsTime.push(item1st.timestamp, item2nd.timestamp, item3rd.timestamp);
  //E lock item
  if (data.sellType == "stop_loss") {
    reduceSlContinuous();
  } else {
    initSlContinuous();
  }
  return order;
}
function analysisAccelerate(prices) {
  let hlPrices = OHLCVformatHightLow(prices);

  let min = Math.min.apply(null, hlPrices);
  let max = Math.max.apply(null, hlPrices);

  let dif = max - min;

  let level = 0;
  if (dif > 0) {
    level = (dif * 100) / min;
  } else if (dif < 0) {
    level = (dif * 100) / max;
  }
  return level;
}

function OHLCVformatHightLow(prices) {
  const hlPrices = prices.reduce((acc, price) => {
    acc.push(price.high);
    acc.push(price.low);
    return acc;
  }, []);
  return hlPrices;
}
//B checking
async function handleCheking() {
  if (data.checkCoinType == "fix") {
    data.checkC = ["NEAR", "DOGE", "SOL", "DOT", "LINK", "XRP", "GALA"];
  } else {
    if (data.allowResetScraping == true) {
      data.allowResetScraping = false;
      await scraping();
    }
  }
  handleCheckC();
  await initItems();
  const trigger = checkVolTrigger();
  if (trigger == true) {
    data.hardBuy = true;
    data.progress = "buying";
    saveData();
  }

  console.log(">>>>> coin", data.coin);
  console.log(">>>>> data.accLv1_2", data.accLv1_2);
  console.log(">>>>> data.accLv1_3", data.accLv1_3);
  //console.log(">>>>> data.accLv2_3", data.accLv2_3);
}
async function scraping() {
  const vgmUrl = "https://www.binance.com/en/markets/spot-USDT";

  await got(vgmUrl)
    .then((response) => {
      const dom = new JSDOM(response.body);
      scrapingRowPushDataCheck(dom);
    })
    .catch((err) => {
      console.log(err);
    });
}
function scrapingRowPushDataCheck(dom) {
  const tableList = dom.window.document.querySelectorAll(
    "#market_trade_list_item"
  );
  let vCoins = [];
  let vCoinsSort = [];
  if (tableList.length > 0) {
    tableList.forEach((item) => {
      if (item.childNodes.length == 7) {
        let vcoin = { spot: "", pc: 0, price: 0 };
        item.childNodes.forEach((node, index) => {
          if (index == 0) {
            vcoin.spot = node.textContent.replace("/USDT", "");
          } else if (index == 1) {
            let tmpPrice = node.textContent.split("/")[0];
            vcoin.price = parseFloat(tmpPrice);
          } else if (index == 2) {
            vcoin.pc = parseFloat(node.textContent.replace("%", ""));
          }
          //console.log("<<<<<<<", index, ": ", node.textContent);
        });
        vCoins.push(vcoin);
      }
    });
  }

  if (vCoins.length > 0) {
    vCoinsSort = bblSortRV(vCoins);
  }
  if (vCoinsSort.length) {
    console.log(">>>>>>>>>>>>>>> vCoins", vCoinsSort);
    pushDataCheckC(vCoinsSort);
  }
}
function pushDataCheckC(vCoinsSort) {
  let checkC = [];
  //vCoinsSort.forEach((item) => {
  vCoinsSort.some(function (item, index) {
    if (item.price < 15 && item.spot.includes("USD") == false) {
      //1.5~15
      //if (configData.coin.toUpperCase() != item.spot.toUpperCase()) {
      checkC.push(item.spot);
      //}
    }
    if (checkC.length >= 15) {
      return true;
    }
  });
  if (checkC.length) {
    data.checkC = checkC;
    console.log(">>>>>>>>>>>>>>> data.checkC", data.checkC);
  }
  return checkC;
}
function bblSortRV(arr) {
  for (var i = 0; i < arr.length; i++) {
    // Last i elements are already in place
    for (var j = 0; j < arr.length - i - 1; j++) {
      // Checking if the item at present iteration
      // is greater than the next iteration
      if (arr[j]["pc"] < arr[j + 1]["pc"]) {
        // If the condition is true then swap them
        var temp = arr[j];
        arr[j] = arr[j + 1];
        arr[j + 1] = temp;
      }
    }
  }
  return arr;
}
function handleCheckC() {
  if (data.checkC.length > data.currentCheckKey + 1) {
    data.coin = data.checkC[data.currentCheckKey];
    data.currentCheckKey += 1;
  } else {
    data.allowResetScraping = true;
    data.coin = data.checkC[0];
    data.currentCheckKey = 0;
  }
}
function checkVolTrigger() {
  let trigger = false;
  //let condTrigger = item1st.status == "up" && data.ma7OrderStt == "up";
  let condTrigger = item1st.status == "up"; //đợi nó up thì quá trễ ròi, item1st.status và vol là đc
  if (condTrigger) {
    data.accLv1_2 = item1st.volume / item2nd.volume;
    data.accLv1_3 = item1st.volume / item3rd.volume;
    data.accLv2_3 = item2nd.volume / item3rd.volume;
    console.log("item1st.volume >>>", item1st.volume);
    console.log("item2nd.volume >>>", item2nd.volume);
    console.log("item3rd.volume >>>", item3rd.volume);
    if (condTrigger) {
      condTrigger = data.accLv1_2 >= 7 ? true : false;
      if (condTrigger == false) {
        condTrigger = data.accLv1_3 >= 7 ? true : false;
      }
    }
  }
  if (condTrigger) {
    trigger = true;
    console.log("item1st >>>", item1st);
    console.log("item2nd >>>", item2nd);
    console.log("item3rd >>>", item3rd);
  }
  return trigger;
}
//E checking
async function loopLoop() {
  stopBot = checkStopBot();
  checkTpSlDay();
  checkStopAmountDay();

  if (stopBot == false) {
    await checkDataSet();
    if (data.progress == "checking") {
      await handleCheking();
    } else {
      await mainHandle();
    }

    startItem += 1;
    data.acc = 0;
    if (data.difPc > 0) {
      data.acc = (data.difPc * 100) / data.tpTimeLoss;
    }
    if (config.tpMode == "percent") {
      if (
        data.hardBuy == true ||
        data.hardSell == true ||
        data.stopBot == true
      ) {
        await dalay(1 * 1000); //1s
      } else if (data.currentPercentAffterBuy >= config.tpMax) {
        await dalay(1 * 1000); //1s
      } else if (item1st.accelerate >= 3 && data.progress == "selling") {
        //Accelerate high
        await dalay(1 * 1000); //1s
      } else if (data.acc >= 80) {
        await dalay(2 * 1000); //2s
      } else {
        await dalay(3 * 1000); //3s
      }
    } else {
      if (
        data.hardBuy == true ||
        data.hardSell == true ||
        data.stopBot == true
      ) {
        await dalay(1 * 1000); //1s
      } else {
        await dalay(3 * 1000); //3s
      }
    }

    loopLoop();
  }
}

async function loopSolf() {
  //update gia tốc, currentPrice item1st mỗi 5s, để đảm bảo trong trường hợp giá tăng giảm mạnh có thể control
  //if != time tức là đã có nến mới, call mainHandle() if lastcall > 10s
}

//node func/tradeCandleV3.js
async function main() {
  await initTopBot();
  //mainHandle()
  await loopLoop();
  //await dalay(5* 1000);
}
main();

//pm2 start func/tradeCandleV3.js --name "tradeCandleV3"
//pm2 start tradeCandleV3
//pm2 stop tradeCandleV3
//pm2 logs tradeCandleV3 --lines 100
//vi config/candleV3/trade.log
//vi config/candleV3/data.json
//tail -n 2000 config/candleV3/trade.log
//tail -n 100 config/candleV3/setting.json
//git restore config/candleV3/data.json
//node func/setData.js stopBot=true app=candleV3
//node func/setData.js hardSell=true app=candleV3

//lý do dừng, rơi vào chu kỳ xuống của chart thì đằng nào cũng thua, ko tránh được
//up ver để luôn theo đỉnh, update mode checking
