const ccxt = require("ccxt");
const moment = require("moment");
let { appDir, privateConfig } = require("../main");

let apiSec = {
  apiKey: privateConfig.API_R,
  secret: privateConfig.SECRET_R,
};
const binance = new ccxt.binance(apiSec);
const myArgs = process.argv.slice(2);

async function checking() {
  let FPrices = await getLastPrice("5m", 7, true);
}
function parseValue(param) {
  let keyValues = param.split("=");
  let key = "";
  let value = "";
  if (keyValues.length == 2) {
    key = keyValues[0];
    value = keyValues[1];
  }

  return value;
}
function getValue() {
  let coin = "ant";
  let time = "5m";
  if (myArgs.length > 0) {
    if (myArgs[0]) {
      let value = parseValue(myArgs[0]);
      if (value != "") {
        coin = value;
      }
    }
    if (myArgs[1]) {
      let value = parseValue(myArgs[1]);
      if (value != "") {
        time = value;
      }
    }
  }
  return { coin: coin, time: time };
}

async function getLastPrice(timeStr, jump, format) {
  let params = getValue();
  let coin = params.coin;
  timeStr = params.time;
  let pairs = coin.toUpperCase() + "/USDT"; //PEOPLE, LIT, WOO, CTK, API3, ALICE, OGN, QNT, BTC, ETH
  if (timeStr == false) {
    timeStr = "1m";
  }
  if (jump === false) {
    jump = 7;
  }
  const prices = await binance.fetchOHLCV(pairs, timeStr, undefined, jump);

  let formating = [];
  if (format === true) {
    formating = formatPrices(prices);
  } else {
    formating = prices;
  }
  console.log(">>>>>>>>>>>> formating", formating);
  console.log(">>>>>>>>>>>> coin/min", coin, timeStr);
  return formating;
}

async function formatPrices(prices) {
  const bPrices = prices.map((price) => {
    return {
      time: price[0],
      timestamp: moment(price[0]).format(),
      open: price[1],
      high: price[2],
      low: price[3],
      close: price[4],
      volume: price[5],
      volumeTo: price[4] * price[5],
      volumeToK: (price[4] * price[5]) / 1000,
      volumeToM: (price[4] * price[5]) / 1000000,
      status:
        price[1] < price[4] ? "up" : price[1] > price[4] ? "down" : "normal",
      change: ((price[4] - price[1]) * 100) / price[1],
      applitude: ((price[2] - price[3]) * 100) / price[3],
      ma7: 0,
      ma7OrderStt: "normal",
    };
  });
  const averagePrice = bPrices.reduce((acc, price) => acc + price.close, 0) / 7;
  const averageApp =
    bPrices.reduce((acc, price) => acc + price.applitude, 0) / 7;
  bPrices[6]["ma7"] = averagePrice;
  bPrices[6]["averageApp"] = averageApp;
  return bPrices;
}

checking();
// data.checkC = ["NEAR", "DOGE", "SOL", "DOT", "LINK", "XRP", "GALA"];
//node func/checkPrice.js

//node func/checkPrice.js coin=btc time=15m
//node func/checkPrice.js coin=eth time=15m
//node func/checkPrice.js coin=shib time=15m
//node func/checkPrice.js coin=chz time=15m
//node func/checkPrice.js coin=DOGE time=15m
//node func/checkPrice.js coin=near time=15m
//node func/checkPrice.js coin=SOL time=15m
//node func/checkPrice.js coin=DOT time=15m
//node func/checkPrice.js coin=LINK time=15m
//node func/checkPrice.js coin=XRP time=15m
//node func/checkPrice.js coin=GALA time=15m
