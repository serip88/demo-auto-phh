//lưu lại data của 1 cặp để chạy backtest

const ccxt = require("ccxt");
const moment = require("moment");
const dalay = require("delay");
const fs = require("fs");
const got = require("got");
const jsdom = require("jsdom");
const { JSDOM } = jsdom;
const { Console } = require("console");

var { config, formatPrices } = require("../../main");
let data = require("./data.json");
let configData = require("../../config/data.json");
data.checkC = ["SHIB", "ETH"];
data.stopBot = false;

const binance = new ccxt.binance({
  apiKey: config.API_R,
  secret: config.SECRET_R,
});
//binance.setSandboxMode(true)
let startItem = 0;
data.currentCheckKey = 0;
data.allowResetScraping = true;
//OGN
var lockItemsTime = [];
async function getLastPrice() {
  pairs = data.coin.toUpperCase() + "/" + data.coinTo.toUpperCase();
  let timeframe = "5m";
  if (data.timeframe) {
    timeframe = data.timeframe;
  }
  const limitRange = 7 + 6;
  const prices = await binance.fetchOHLCV(
    pairs,
    timeframe,
    undefined,
    limitRange
  );
  const bPrices = formatPrices(prices, lockItemsTime);

  return bPrices;
}

function checkVolTrigger() {
  let trigger = false;
  //let condTrigger = item1st.status == "up" && data.ma7OrderStt == "up";
  let condTrigger = item1st.status == "up"; //đợi nó up thì quá trễ ròi, item1st.status và vol là đc

  data.accLv1_2 = item1st.volume / item2nd.volume;
  data.accLv1_3 = item1st.volume / item3rd.volume;
  data.accLv2_3 = item2nd.volume / item3rd.volume;
  if (condTrigger) {
    condTrigger = data.accLv1_2 >= 7 ? true : false;
    if (condTrigger == false) {
      condTrigger = data.accLv1_3 >= 7 ? true : false;
    }
  }
  if (condTrigger) {
    trigger = true;
  }
  return trigger;
}
function checkVolTriggerV2() {
  let trigger = false;
  let condTrigger = item1st.status == "up";
  // ignore condi data.ma7OrderStt == "up" should buy soon

  data.accLv1_2 = item1st.volume / item2nd.volume;
  data.accLv1_3 = item1st.volume / item3rd.volume;
  data.accLv2_3 = item2nd.volume / item3rd.volume;
  if (condTrigger) {
    condTrigger = data.accLv1_2 >= 2 ? true : false;
    if (condTrigger == false) {
      condTrigger = data.accLv1_3 >= 2 ? true : false;
    }
  }
  if (condTrigger) {
    trigger = true;
  }
  return trigger;
}
function setData(fPrices) {
  item1st = fPrices[0];
  item2nd = fPrices[1];
  item3rd = fPrices[2];
  item4th = fPrices[3];
  item5th = fPrices[4];
  item6th = fPrices[5];
  item7th = fPrices[6];

  itemTop = false;
  itemBot = false;

  //B handle set data
  data.currentPrice = item1st.close;

  //B handle ma7
  data.ma7 = item1st.ma7;
  if (data.currentPrice > data.ma7) {
    data.ma7OrderStt = "up";
  } else if (data.currentPrice < data.ma7) {
    data.ma7OrderStt = "down";
  } else {
    data.ma7OrderStt = "same";
  }
  //E handle ma7

  //E handle set data
}
function getLocalDateString() {
  var today = new Date();
  let locDate = today.toLocaleDateString();
  let timeString = today.toTimeString().split(" ")[0];
  let locDateString = [locDate, timeString].join(" ");
  return locDateString;
}
async function setTrigger() {
  //data.timeFetchTmp = 20;
  await fs.readFile("./config/data.json", "utf8", function (err, dataEncode) {
    //B add log
    if (err) {
      console.log("readFile get problem");
      throw err;
    }
    let dataDecode = JSON.parse(dataEncode);
    let locDateString = getLocalDateString();
    let triggerLog = {
      time: locDateString,
      coin: data.coin,
      progress: dataDecode.progress,
      hardBuy: dataDecode.hardBuy,
      stt: "not trigger",
      close: item1st.close,
      accLv1_2: data.accLv1_2,
      accLv1_3: data.accLv1_3,
      timestamp: item1st.timestamp,
    };
    //E add log

    if (dataDecode.progress == "buying" && dataDecode.hardBuy == false) {
      //hardBuy == false it's mean not rewrite
      dataDecode.hardBuy = true;
      //dataDecode.waitHardBuy = true;
      dataDecode.coin = data.coin;
      dataDecode.tpMin = 2;
      //dataDecode.slContinuous = 3;
      saveConfigData(dataDecode);
      triggerLog.stt = "trigger";
    }

    //B check and set logTrigger
    let exist = arrCheckKeyExist(data.triggered, triggerLog.coin);
    if (exist == false) {
      data.triggered[triggerLog.coin] = triggerLog.timestamp;
      logTrigger.log(triggerLog);
    } else {
      let time1stTriggered = new Date(data.triggered[triggerLog.coin]);
      let time2ndCurrent = new Date(triggerLog.timestamp);
      time1stTriggered.setMinutes(time1stTriggered.getMinutes() + 5);
      if (time1stTriggered.getTime() < time2ndCurrent.getTime()) {
        data.triggered[triggerLog.coin] = triggerLog.timestamp;
        logTrigger.log(triggerLog);
      }
    }
    //E check and set logTrigger
  });
}

function arrCheckKeyExist(arr, key) {
  if (typeof arr[key] == "undefined") {
    return false;
  } else {
    return true;
  }
}
const logTrigger = new Console({
  stdout: fs.createWriteStream("./func/checkTriggerBuy/trigger.log", {
    flags: "a",
  }),
});

async function saveConfigData(dataSave) {
  configData = dataSave; //update config data
  callBack = function (err) {
    if (err) throw err;
    console.log("saveConfigData callBack completed");
  };
  await fs.writeFile(
    "./config/data.json",
    JSON.stringify(dataSave),
    "utf8",
    callBack
  );
}
async function saveData(dataSave) {
  callBack = function (err) {
    if (err) throw err;
    console.log("saveData callBack completed");
  };
  await fs.writeFile(
    "./func/checkTriggerBuy/data.json",
    JSON.stringify(dataSave),
    "utf8",
    callBack
  );
}
async function scraping() {
  const vgmUrl = "https://www.binance.com/en/markets/spot-USDT";

  await got(vgmUrl)
    .then((response) => {
      const dom = new JSDOM(response.body);
      scrapingRowPushDataCheck(dom);
    })
    .catch((err) => {
      console.log(err);
    });
}
function scrapingRowPushDataCheck(dom) {
  const tableList = dom.window.document.querySelectorAll(
    "#market_trade_list_item"
  );
  let vCoins = [];
  let vCoinsSort = [];
  if (tableList.length > 0) {
    tableList.forEach((item) => {
      if (item.childNodes.length == 7) {
        let vcoin = { spot: "", pc: 0, price: 0 };
        item.childNodes.forEach((node, index) => {
          if (index == 0) {
            vcoin.spot = node.textContent.replace("/USDT", "");
          } else if (index == 1) {
            let tmpPrice = node.textContent.split("/")[0];
            vcoin.price = parseFloat(tmpPrice);
          } else if (index == 2) {
            vcoin.pc = parseFloat(node.textContent.replace("%", ""));
          }
          //console.log("<<<<<<<", index, ": ", node.textContent);
        });
        vCoins.push(vcoin);
      }
    });
  }

  if (vCoins.length > 0) {
    vCoinsSort = bblSortRV(vCoins);
  }
  if (vCoinsSort.length) {
    console.log(">>>>>>>>>>>>>>> vCoins", vCoinsSort);
    pushDataCheckC(vCoinsSort);
  }
}
function pushDataCheckC(vCoinsSort) {
  let checkC = [];
  //vCoinsSort.forEach((item) => {
  vCoinsSort.some(function (item, index) {
    if (item.price < 15 && item.spot.includes("USD") == false) {
      //1.5~15
      if (configData.coin.toUpperCase() != item.spot.toUpperCase()) {
        checkC.push(item.spot);
      }
    }
    if (checkC.length > 10) {
      return true;
    }
  });
  if (checkC.length) {
    data.checkC = checkC;
    console.log(">>>>>>>>>>>>>>> data.checkC", data.checkC);
  }
  return checkC;
}
function bblSort(arr) {
  for (var i = 0; i < arr.length; i++) {
    // Last i elements are already in place
    for (var j = 0; j < arr.length - i - 1; j++) {
      // Checking if the item at present iteration
      // is greater than the next iteration
      if (arr[j]["pc"] > arr[j + 1]["pc"]) {
        // If the condition is true then swap them
        var temp = arr[j];
        arr[j] = arr[j + 1];
        arr[j + 1] = temp;
      }
    }
  }
  return arr;
}
function bblSortRV(arr) {
  for (var i = 0; i < arr.length; i++) {
    // Last i elements are already in place
    for (var j = 0; j < arr.length - i - 1; j++) {
      // Checking if the item at present iteration
      // is greater than the next iteration
      if (arr[j]["pc"] < arr[j + 1]["pc"]) {
        // If the condition is true then swap them
        var temp = arr[j];
        arr[j] = arr[j + 1];
        arr[j + 1] = temp;
      }
    }
  }
  return arr;
}
function scrapingClass2(dom) {
  const tableList = dom.window.document.querySelectorAll(".css-1r1yofv");
  if (tableList.length > 0) {
    tableList.forEach((item) => {
      item.childNodes.forEach((node) => {
        console.log("<<<<<<<", node.textContent);
      });
    });
  }
}
function scrapingClass(dom) {
  const tableList = dom.window.document.querySelectorAll(".css-1sud65k");
  if (tableList.length > 0) {
    tableList.forEach((link) => {
      console.log(">>>>>>>", link.href);
    });
  }
}
function handleCheckC() {
  if (data.checkC.length > data.currentCheckKey) {
    data.coin = data.checkC[data.currentCheckKey];
    data.currentCheckKey += 1;
  } else {
    data.allowResetScraping = true;
    data.coin = data.checkC[0];
    data.currentCheckKey = 1;
  }
}
async function checkUpdateData() {
  await fs.readFile(
    "./func/checkTriggerBuy/data.json",
    "utf8",
    function (err, dataEncode) {
      let dataDecode = JSON.parse(dataEncode);
      if (dataDecode.stopBot == true) {
        data.stopBot = true;
      }
    }
  );
}
async function mainHandle() {
  await checkUpdateData();
  if (data.stopBot == true) {
    return;
  }
  if (data.checkCoinType == "fix") {
    data.checkC = ["NEAR", "DOGE", "SOL", "DOT", "LINK", "XRP", "GALA"];
  } else {
    if (data.allowResetScraping == true) {
      data.allowResetScraping = false;
      await scraping();
    }
  }
  handleCheckC();

  const fPrices = await getLastPrice();
  setData(fPrices);
  const trigger = checkVolTrigger();
  if (trigger == true) {
    await setTrigger();
  }
  // console.log(">>>>>1 fPrices", fPrices);
  // console.log(">>>>>2 data", data);
  // console.log(">>>>>3 trigger", trigger);
  console.log(">>>>> startItem", startItem, " ", getLocalDateString());
  console.log(">>>>> coin", data.coin);
  console.log(">>>>> data.accLv1_2", data.accLv1_2);
  console.log(">>>>> data.accLv1_3", data.accLv1_3);
  //console.log(">>>>> data.accLv2_3", data.accLv2_3);
}

async function loopLoop() {
  while (!data.stopBot) {
    await mainHandle();
    startItem += 1;
    let timeFetch = data.timeFetch ? data.timeFetch : 10;
    await dalay(timeFetch * 1000);
  }
  if (data.stopBot == true) {
    data.stopBot = false;
  }
  await saveData(data);
}
//node func/checkTriggerBuy/index.js

//npm install pm2 -g //install
//pm2 start func/checkTriggerBuy/index.js --name "checkTriggerbuy"
//pm2 stop func/checkTriggerBuy/index.js --name "checkTriggerbuy"
//pm2 start checkTriggerbuy
//pm2 stop checkTriggerbuy
//pm2 logs checkTriggerbuy // show logs
//pm2 delete checkTriggerbuy
//pm2 ls // show list process
//vi func/checkTriggerBuy/trigger.log
//tail -f func/checkTriggerBuy/trigger.log
//tail -n 50 func/checkTriggerBuy/trigger.log

//cần thay thế dòng 16 và 39
//hàm cho phép lấy log của 1 coin
//getLastPrice();
//scraping();
loopLoop();

/** TODO
 * dò 5p nếu độ chênh lệch v cao x7-> thì kích hoạt mua
 * affterBuyHighPrice: 0.02989,
  affterBuyLowPrice: 0.02365,
  buyPrice: 0.02418,

  side: 'sell',
  price: 0.02853,
  tct => TP difPc:  17.99007444168734
  => https://drive.google.com/file/d/1wdJAAonQU2jDTe1NgrINyrDVkTrC1CBH/view?usp=sharing

  - tập trung vào: item1st.volume applitude status  


  2. đổi ý: chỉ tập trung check những đồng uy tín như near, lỡ leo đỉnh còn xuống được, xét min max đồ thị để ko leo lố đỉnh
  near ưu tiên check v trước, nếu trên 1m và đồ thị up thì quạt 1 lần xong stop
  => giảm thiểu quy trình đánh 1 đồng trước
 * **/
