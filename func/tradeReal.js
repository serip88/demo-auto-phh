const dalay = require("delay");
const config = require("../config/setting.json");

let data = require("../config/data.json");
let pairs = config.coin.toUpperCase() + "/" + config.coinTo.toUpperCase(); //TRX/USDT

let { appDir, privateConfig, formatPrices } = require("../main");
const fs = require("fs");
const ccxt = require("ccxt");
let apiSec = {
  apiKey: privateConfig.API_R,
  secret: privateConfig.SECRET_R,
};
if (config.real === true) {
  console.log(">>>>>>>>>>>> config.real", config.real);
  apiSec.apiKey = privateConfig.API_SM;
  apiSec.secret = privateConfig.SECRET_SM;
}
const binance = new ccxt.binance(apiSec);
//binance.setSandboxMode(true);

//coin = tct
//timeStr = "1m" to fetch
//timeStr = "4h" to get min max graph
async function getLastPrice(timeStr, jump, format) {
  pairs = data.coin.toUpperCase() + "/" + config.coinTo.toUpperCase();
  if (timeStr === false) {
    timeStr = "1m";
  }
  if (jump === false) {
    jump = 7;
  }
  if (format === true) {
    jump = 7 + 6;
  }
  const prices = await binance.fetchOHLCV(pairs, timeStr, undefined, jump);
  if (format === true) {
    const bPrices = formatPrices(prices, lockItemsTime);

    return bPrices;
  } else {
    return prices;
  }
}
let item1st = false;
let item2nd = false;
let item3rd = false;
let item4th = false;
let item5th = false;
let item6th = false;

let lockItemsTime = [];

let level1m = 0;
let level2m = 0;
let level3m = 0;
let level4m = 0;
let level5m = 0;
let level6m = 0;

//======= test param
let stopBot = false;
let startItem = 0;

async function mainHandle() {
  data.commentB_debug = [];
  //B update realtime params: hardBuy if data == buying
  if (data.progress == "buying") {
    fs.readFile("./config/data.json", "utf8", function (err, dataEncode) {
      let dataDecode = JSON.parse(dataEncode);
      if (data.hardBuy != dataDecode.hardBuy) {
        data.hardBuy = dataDecode.hardBuy;
      }
      if (data.coin != dataDecode.coin) {
        data.coin = dataDecode.coin;
      }
      if (data.waitHardBuy != dataDecode.waitHardBuy) {
        data.waitHardBuy = dataDecode.waitHardBuy;
      }
      if (data.tpMin != dataDecode.tpMin) {
        //set from trigger, first time get from config.tpMin
        data.tpMin = dataDecode.tpMin;
      }
      if (data.stopAfterSell != dataDecode.stopAfterSell) {
        data.stopAfterSell = dataDecode.stopAfterSell;
      }
      if (data.hardBuy_cmt != dataDecode.hardBuy_cmt) {
        data.hardBuy_cmt = dataDecode.hardBuy_cmt;
      }
      if (dataDecode.stopBot == true) {
        data.stopBot = true;
      }
      if (data.timeLossTmp > 0 && data.tpQuick != dataDecode.tpQuick) {
        data.tpQuick = dataDecode.tpQuick;
      }
      // Display the file content
      //console.log(">>>>>>> data.hardBuy_cmt", data);
    });
  } else {
    //selling
    fs.readFile("./config/data.json", "utf8", function (err, dataEncode) {
      let dataDecode = JSON.parse(dataEncode);
      if (dataDecode.stopBot == true) {
        data.stopBot = true;
      }
      if (dataDecode.hardSell == true) {
        data.hardSell = true;
      }
      if (data.stopAfterSell != dataDecode.stopAfterSell) {
        data.stopAfterSell = dataDecode.stopAfterSell;
      }
      if (data.tpQuick != dataDecode.tpQuick) {
        data.tpQuick = dataDecode.tpQuick;
      }
      if (data.tpMin != dataDecode.tpMin) {
        //set from trigger, first time get from config.tpMin
        data.tpMin = dataDecode.tpMin;
      }
    });
  }
  if (data.stopBot == true) {
    return;
  }
  if (data.waitHardBuy == true && data.hardBuy == false) {
    console.log(">>>>>>> startItem", startItem);
    console.log(">>>>>>> waitHardBuy hardBuy", data.hardBuy);
    //after sell will wait for hardBuy, sell one time
    return;
  }
  //E update realtime params: hardBuy if data == buying
  pairs = data.coin.toUpperCase() + "/" + config.coinTo.toUpperCase();
  let isInit = await initItems();
  if (isInit) {
    await checkTrade();
  }
}

function checkStopBot() {
  let status = false;
  if (config.slContinuousOn) {
    if (data.slContinuous <= 0) {
      status = true;
    } else {
      status = false;
    }
  } else {
    status = false;
  }
  if (data.stopBot == true) {
    status = true;
  }
  if (status == true) {
    data.stopBot = false;
    saveData();
  }
  return status;
}
function reduceSlContinuousTmp() {
  //reduce slContinuous after SL
  let allowSell = false;
  data.slContinuousTmp = data.slContinuousTmp - 1;
  const timeLossTmp = config.slContinuousTmp - data.slContinuousTmp;
  if (data.slContinuousTmp > 0) {
    data.progress = "buying";
    //data.hardBuy = true; consider about this
  } else {
    allowSell = true;
  }
  data.timeLossTmp = timeLossTmp;
  return allowSell;
}
function reduceSlContinuous() {
  //reduce slContinuous after SL
  data.slContinuous = data.slContinuous - 1;
  if (data.slContinuous <= 0 && config.buyMode == "trigger") {
    data.maxTriggerLoss = data.maxTriggerLoss - 1;
    if (data.maxTriggerLoss > 0) {
      data.waitHardBuy = true;
      initSlContinuous(); //reset slContinuous
    }
  }
}

function initSlContinuous() {
  //init slContinuous for first time or affter TP
  data.slContinuous = config.slContinuous;
  data.slContinuousTmp = config.slContinuousTmp;
  data.timeLossTmp = 0;
}
async function initTopBot() {
  //run first time
  data.items = [];
  if (config.checkMa7BuyUp) {
    data.flagMa7BuyUp = true;
  }
  //importance cause it reset slContinuous/slContinuousTmp on progress
  if (data.progress == "buying" && data.timeLossTmp == 0) {
    initSlContinuous();
  }
  //let purePrices = dataLogPure;
  //108 = 1 man hinh mac 13
  if (data.progress == "selling" && data.waitHardBuy == true) {
    //keep data.tpMin
  } else {
    data.tpMin = config.tpMin; //set tpMin
  }

  let purePrices = await getLastPrice(
    config.timeframeBotTop,
    config.fetchBotTopLimit,
    false
  );
  const hlPrices = purePrices.reduce((acc, price) => {
    acc.push(price[2]);
    acc.push(price[3]);
    return acc;
  }, []);
  let min = Math.min.apply(null, hlPrices);
  let max = Math.max.apply(null, hlPrices);
  data.chartTop = max;
  data.chartBottom = min;

  console.log(">>>>min max", min, max);
  return hlPrices;
}
async function initItems() {
  fPrices = await getLastPrice(config.timeframeRun, 7, true);

  item1st = fPrices[0];
  item2nd = fPrices[1];
  item3rd = fPrices[2];
  item4th = fPrices[3];
  item5th = fPrices[4];
  item6th = fPrices[5];
  item7th = fPrices[6];

  itemTop = false;
  itemBot = false;

  //B handle set data
  data.currentPrice = item1st.close;

  //B handle ma7
  data.ma7 = item1st.ma7;
  if (data.currentPrice > data.ma7) {
    data.ma7OrderStt = "up";
  } else if (data.currentPrice < data.ma7) {
    data.ma7OrderStt = "down";
  } else {
    data.ma7OrderStt = "normal";
  }
  //E handle ma7

  //set data.chartTop/bot
  if (data.currentPrice > data.chartTop) {
    data.chartTop = data.currentPrice;
    itemTop = item1st;
  }
  if (data.currentPrice < data.chartBottom) {
    data.chartBottom = data.currentPrice;
    itemBot = item1st;
  }

  data.currentChart = "normal";
  if (data.progress == "selling") {
    //set affterBuyHighPrice, affterBuyLowPrice affter buy
    /*if (data.currentPrice > data.affterBuyHighPrice) {
      data.affterBuyHighPrice = data.currentPrice
    }
    if (data.currentPrice < data.affterBuyLowPrice) {
      data.affterBuyLowPrice = data.currentPrice
    }*/
    if (item1st.high > data.affterBuyHighPrice) {
      data.affterBuyHighPrice = item1st.high;
    }
    if (item1st.low < data.affterBuyLowPrice) {
      data.affterBuyLowPrice = item1st.low;
    }

    // let aUnit = (data.affterBuyHighPrice - data.affterBuyLowPrice) / 100
    // data.currentPercentAffterBuy = (data.currentPrice - data.affterBuyLowPrice) / aUnit
    data.currentPercentAffterBuy = 0;
    if (data.currentPrice > data.buyPrice) {
      let aUnit = (data.affterBuyHighPrice - data.buyPrice) / 100;
      data.currentPercentAffterBuy =
        (data.currentPrice - data.buyPrice) / aUnit;
    } else if (data.currentPrice < data.buyPrice) {
      data.currentPercentAffterBuy =
        (data.currentPrice * 100) / data.buyPrice - 100;
    }
    //set currentChart
    if (data.currentPrice > data.buyPrice) {
      data.currentChart = "up";
    } else if (data.currentPrice < data.buyPrice) {
      data.currentChart = "down";
    }
  }
  //E handle set data

  // item1st.status = checkUpDown(item1st, item2nd)
  // item2nd.status = checkUpDown(item2nd, item3rd)
  // item3rd.status = checkUpDown(item3rd, item4th)
  // item4th.status = checkUpDown(item4th, item5th)
  // item5th.status = checkUpDown(item5th, item6th)

  let prices = [];
  prices[0] = item1st;
  prices[1] = item2nd;
  level1m = await analysisAccelerate(prices);
  item1st.accelerate = level1m;
  handlePushItems(item1st);

  prices = [];
  prices[0] = item1st;
  prices[1] = item3rd;
  level2m = await analysisAccelerate(prices);
  item2nd.accelerate = level2m;

  prices = [];
  prices[0] = item1st;
  prices[1] = item4th;
  level3m = await analysisAccelerate(prices);
  item3rd.accelerate = level3m;

  prices = [];
  prices[0] = item1st;
  prices[1] = item5th;
  level4m = await analysisAccelerate(prices);
  item4th.accelerate = level4m;

  prices = [];
  prices[0] = item1st;
  prices[1] = item6th;
  level5m = await analysisAccelerate(prices);
  item5th.accelerate = level5m;

  prices = [];
  prices[0] = item1st;
  prices[1] = item7th;
  level6m = await analysisAccelerate(prices);
  item6th.accelerate = level6m;

  console.log("level 0 >>>>", item1st);
  console.log("level 1 >>>>", level1m, item2nd);
  console.log("level 2 >>>>", level2m, item3rd);
  console.log("level 3 >>>>", level3m, item4th);
  console.log("level 4 >>>>", level4m, item5th);
  console.log("level 5 >>>>", level5m, item6th);
  console.log("level 6 >>>>", level6m, item7th);

  let accelerates = [];
  let tmpAcc = 0;

  tmpAcc =
    item1st.status == "down" ? item1st.accelerate * -1 : item1st.accelerate;
  accelerates.push(tmpAcc);
  tmpAcc =
    item2nd.status == "down" ? item2nd.accelerate * -1 : item2nd.accelerate;
  accelerates.push(tmpAcc);
  tmpAcc =
    item3rd.status == "down" ? item3rd.accelerate * -1 : item3rd.accelerate;
  accelerates.push(tmpAcc);
  tmpAcc =
    item4th.status == "down" ? item4th.accelerate * -1 : item4th.accelerate;
  accelerates.push(tmpAcc);
  tmpAcc =
    item5th.status == "down" ? item5th.accelerate * -1 : item5th.accelerate;
  accelerates.push(tmpAcc);
  tmpAcc =
    item6th.status == "down" ? item6th.accelerate * -1 : item6th.accelerate;
  accelerates.push(tmpAcc);

  data.accelerates = accelerates.join(", ");
  return true;
}

function handlePushItems(currentItem) {
  var today = new Date();
  let locDate = today.toLocaleDateString();
  let timeString = today.toTimeString().split(" ")[0];
  let locDateString = [locDate, timeString].join(" ");
  let item = {
    time: locDateString,
    status: "same",
    dif: 0,
    price: currentItem.close,
    volume: currentItem.volume,
    volumeTo: currentItem.volumeTo,
  };
  if (data.items.length > 0) {
    let lastIndex = data.items.length - 1;
    let lastItemEn = data.items[lastIndex];
    let lastItem = JSON.parse(lastItemEn);
    item.dif = item.price - lastItem.price;
    if (lastItem.price < item.price) {
      item.status = "up";
    } else if (lastItem.price > item.price) {
      item.status = "down";
    }
  }
  //data.items.push(item);
  data.items.push(JSON.stringify(item));
  if (data.items.length > 10) {
    data.items.shift();
  }
}

function checkUpDown(first, second) {
  let status = "normal";
  if (first.close > second.close) {
    status = "up";
  } else if (first.close < second.close) {
    status = "down";
  }
  return status;
}

function getNextPercentTP(tpMin, timeLoss) {
  //tpMin=2
  //timeLoss: 0~3
  //tpMinNew: 2,3,4,5
  let tpMinNew = (tpMin / 2) * timeLoss + tpMin;
  return tpMinNew;
}
function checkVolChange() {
  let stt = false;
  if (item1st.status == "up" && item2nd.status == "up") {
    if (item1st.volume > item2nd.volume * 2) {
      stt = true;
    }
  } else if (item1st.status == "up" && item2nd.status == "down") {
    if (item1st.volume > item2nd.volume * 2) {
      stt = true;
    }
  }
  return stt;
}

function checkAccFlagMa7Up() {
  let ignoreFlagMa7BuyUp = false;
  //affter tp still up, check .acc
  if (item1st.status == "up" && item1st.accelerate > 6) {
    ignoreFlagMa7BuyUp = true;
  }
  return ignoreFlagMa7BuyUp;
}

function handleCheckMa7BuySoon() {
  let allowBuy = false;

  if (data.ma7OrderStt == "down" || item1st.low < data.ma7) {
    data.flagMa7BuyUp = false; //= true if checkMa7BuyUp == true and Sell TP
  }
  const modeMa7 = "low"; //low/any
  let allowCheckVol = false;
  if (modeMa7 == "low") {
    if (data.ma7OrderStt == "down") {
      allowCheckVol = true;
    }
  } else {
    //any
    allowCheckVol = true;
  }
  const cond3d2u1u =
    item3rd.status == "down" &&
    item2nd.status == "up" &&
    item1st.status == "up";

  const ratioVol = 1.8;
  if (allowCheckVol) {
    const cond2d1u = item2nd.status == "down" && item1st.status == "up";

    const cond3u2u1u =
      item3rd.status == "up" &&
      item2nd.status == "up" &&
      item1st.status == "up";
    //2D,1U
    if (cond2d1u) {
      let accLv1_2 = item1st.volume / item2nd.volume;
      if (accLv1_2 >= ratioVol) {
        allowBuy = true;
      }
    }
    //3D,2U,1U
    else if (cond3d2u1u) {
      let accLv12_3 = (item1st.volume + item2nd.volume) / item3rd.volume;
      if (accLv12_3 >= ratioVol) {
        allowBuy = true;
      }
    }
    //3U,2U,1U
    else if (cond3u2u1u) {
      let accLv12_3 = (item1st.volume + item2nd.volume) / item3rd.volume;
      if (accLv12_3 >= ratioVol) {
        allowBuy = true;
      }
    }
  } else if (data.ma7OrderStt == "up") {
    //3D,2U,1U
    if (cond3d2u1u) {
      let accLv12_3 = (item1st.volume + item2nd.volume) / item3rd.volume;
      if (accLv12_3 >= ratioVol) {
        allowBuy = true;
      }
    }
  }
  return allowBuy;
}
function handleCheckMa7BuySoonMa3() {
  if (data.ma7OrderStt == "down" || item1st.low < data.ma7) {
    data.flagMa7BuyUp = false; //= true if checkMa7BuyUp == true and Sell TP
  }
  let allowBuy = false;
  if (
    item1st.status == "up" &&
    item1st.ma3 >= item2nd.ma3 &&
    data.flagMa7BuyUp == false
  ) {
    if (item1st.volume > item2nd.volume * 1.2) {
      allowBuy = true;
    }
  }
  return allowBuy;
}

async function checkTrade() {
  //buying/selling
  data.chartBottomLow =
    ((data.chartTop - data.chartBottom) / 100) * 5 + data.chartBottom;
  //chartBottomLow = chartBottom + 5%
  data.chartBottomHigh =
    ((data.chartTop - data.chartBottom) / 100) * 10 + data.chartBottom;
  //chartBottomHigh = chartBottom + 10%
  data.chartTopHigh =
    ((data.chartTop - data.chartBottom) / 100) * 95 + data.chartBottom;
  data.chartTopLow =
    ((data.chartTop - data.chartBottom) / 100) * 90 + data.chartBottom;
  console.log(" ---------- ");
  console.log(">>>>>> data.chartBottomHigh: ", data.chartBottomHigh);
  console.log(">>>>>> data.chartBottomLow: ", data.chartBottomLow);
  console.log(">>>>>> data.chartBottom: ", data.chartBottom);

  console.log(" ---------- ");
  console.log(">>>>>> data.chartTop: ", data.chartTop);
  console.log(">>>>>> data.chartTopHigh: ", data.chartTopHigh);
  console.log(">>>>>> data.chartTopLow: ", data.chartTopLow);
  console.log(" ---------- ");
  if (config.onlyCheck == false) {
    if (data.progress == "buying") {
      //item2nd.change > 0.4 bị dư, phòng trường hợp call api chậm, nếu fetch realtime thì bỏ qua
      if (data.hardBuy == true) {
        data.hardBuy = false;
        data.waitHardBuy = false;
        data.buyType = "hard_buy_coin: " + data.coin;
        console.log(">>>>>> marketBuy");
        order = await handleMarketBuy(item1st.close);
      } else if (config.checkMa3Buy == true) {
        data.commentB = "checkMa3Buy ...";
        let allowBuy = handleCheckMa7BuySoonMa3();
        if (allowBuy == true) {
          data.buyType = "checkMa3Buy";
          console.log(">>>>>> marketBuy");
          order = await handleMarketBuy(item1st.close);
        }
      } else if (config.checkMa7BuySoon == true) {
        data.commentB = "checkMa7BuySoon ...";
        let allowBuy = false;
        allowBuy = handleCheckMa7BuySoon();
        data.commentB_debug = ["allowBuy ...", allowBuy];

        if (allowBuy == true) {
          data.buyType = "checkMa7BuySoon";
          console.log(">>>>>> marketBuy");
          order = await handleMarketBuy(item1st.close);
        }
      } else if (config.checkMa7BuyUp == true) {
        //B handle checkMa7BuyUp
        let checkMa7BuyUp = 0;
        if (data.ma7OrderStt == "down" || item1st.low < data.ma7) {
          data.flagMa7BuyUp = false; //= true if checkMa7BuyUp == true and Sell TP
        }
        let ignoreFlagMa7BuyUp = false;
        if (
          data.ma7OrderStt == "up" &&
          item1st.status == "up" &&
          item1st.lockItem == 0
        ) {
          if (data.flagMa7BuyUp == true) {
            //B check ignoreFlagMa7BuyUp
            //safe thì ko nên đu đỉnh
            //ignoreFlagMa7BuyUp = checkAccFlagMa7Up();

            //E check ignoreFlagMa7BuyUp
            if (ignoreFlagMa7BuyUp == true) {
              checkMa7BuyUp = 1;
            }
          } else {
            checkMa7BuyUp = 1;
          }
        } else {
          checkMa7BuyUp = 0;
        }
        //E handle checkMa7BuyUp
        //B update cond safe to B: vol, change
        if (checkMa7BuyUp && ignoreFlagMa7BuyUp == false) {
          const isSafe = checkVolChange();
          checkMa7BuyUp = isSafe;
        }
        //E update cond safe to B
        if (checkMa7BuyUp) {
          data.buyType = "checkMa7BuyUp";
          console.log(">>>>>> marketBuy");
          order = await handleMarketBuy(item1st.close);
        } else {
          data.comment = "checkMa7BuyUp failse lockItem:" + item1st.lockItem;
        }
      } else if (
        item1st.lockItem == 0 &&
        item2nd.lockItem == 0 &&
        item1st.status == "up" &&
        (item2nd.change > 0.4 || item1st.change > 0.4)
      ) {
        //trường hợp tăng đột xuất, mua đu đỉnh
        //https://drive.google.com/file/d/11uQkq69c7Q8_RNeRE3OuE6mYFyBrrvvo/view?usp=sharing
        //https://drive.google.com/drive/folders/15RN3WceEFvMVZ3m90Ne3f4N1noDegfkz?ths=true
        data.buyType = "du_dinh";

        if (
          item1st.volume < item2nd.volume * 2 &&
          item1st.volume > item2nd.volume * 1.8 &&
          item1st.applitude > item2nd.applitude * 2 &&
          item1st.status == "up" &&
          item2nd.status != "down"
        ) {
          //fix case buy later
          //https://drive.google.com/drive/folders/1l9LxvW6pjkyalKHk0MXRPLNCSeOtBWCG?usp=sharing

          data.buyType = "du_dinh_volume_applitude";
          console.log(">>>>>> marketBuy");
          order = await handleMarketBuy(item1st.close);
        } else if (
          item1st.volume < item2nd.volume * 2 &&
          item3rd.status == "down" &&
          item3rd.change < -1 &&
          item1st.status == "up" &&
          item2nd.status == "up"
        ) {
          //buy=> https://drive.google.com/file/d/1P2A9WQbhkSdQ3n3XlwSVftnP4l8u-BQh/view?usp=sharing
          //sell => https://drive.google.com/file/d/1KGiBclHa-Hoo390_1rPWb_EJePePbtWL/view?usp=sharing
          //include cond checkMa7BuyUp
          //=> phe ban phai ban that nhieu de ko con luc day xuong
          if (
            item3rd.volume > item2nd.volume &&
            item3rd.volume > item1st.volume &&
            checkMa7BuyUp
          ) {
            data.buyType = "du_dinh_volume_down_up_up";
            console.log(">>>>>> marketBuy");
            order = await handleMarketBuy(item1st.close);
          }
        } else if (item1st.volume > item2nd.volume * 2 && checkMa7BuyUp) {
          data.buyType = "du_dinh_volume";
          console.log(">>>>>> marketBuy");
          order = await handleMarketBuy(item1st.close);
        } else if (
          item1st.lockItem == 0 &&
          item2nd.lockItem == 0 &&
          item1st.status == "up" &&
          item2nd.status == "up" &&
          item3rd.status == "down" &&
          (item2nd.change > 0.5 || item3rd.change < -1)
        ) {
          if ((item2nd.volume / 100) * 85 < item1st.volume && checkMa7BuyUp) {
            //update to check case giảm mạnh sau đó có xu hướng tăng mạnh
            //https://drive.google.com/file/d/1Z7Z6jGftnzZKKfWZlv6cvh6I0UD_aTuu/view?usp=sharing
            data.buyType = "du_dinh_co_node_giam";
            console.log(">>>>>> marketBuy");
            order = await handleMarketBuy(item1st.close);
          } else {
            //ignore
            //case item2nd.volume > item1st.volume, ignore it, because chart will down
            //https://drive.google.com/file/d/1y1d1nR5P91pxMjmbUO1NP4ISvL04SMME/view?usp=sharing
          }
        }
        //}else if(item2nd.status == "up" && (item3rd.change < -0.4 || item4th.change < -0.4 || item5th.change < -0.4 ) && (item3rd.status == "down" || item4th.status == "down" || item5th.status == "down") && (data.chartBottomLow < data.currentPrice  && data.currentPrice < data.chartBottomHigh) ){
      } /*else if (
        item1st.lockItem == 0 &&
        item2nd.lockItem == 0 &&
        item2nd.status == "up" &&
        (item3rd.change < -0.4 ||
          item4th.change < -0.4 ||
          item5th.change < -0.4) &&
        (item3rd.status == "down" ||
          item4th.status == "down" ||
          item5th.status == "down") &&
        data.chartBottomLow < data.currentPrice
      ) {
        //item thu 2 có dấu hiệu tăng, item 3/4/5 giảm mạnh và current phải chạm đáy trong khoảng 90-95% -> trường hợp giảm liên tục, dò đáy
        //https://drive.google.com/file/d/1wtRLMCWd5va8YR5CdlCv-gjLVTxXO3R7/view?usp=sharing
        //https://drive.google.com/file/d/1IvNFPNgg7BjUxbqN5BiRQ02ZglIvXMQD/view?usp=sharing
        // tạm thời khóa vì trường hợp này lãi ít, có khả năng lỗ
        console.log(">>>>>> marketBuy");
        data.buyType = "do_day";
        logTrade.log(">>>>> data ", data);
        order = await handleMarketBuy(item1st.close);
        
        updateBuyData();
      } */ else if (
        item1st.lockItem == 0 &&
        item2nd.lockItem == 0 &&
        item2nd.status == "up" &&
        (item3rd.change < -0.4 ||
          item4th.change < -0.4 ||
          item5th.change < -0.4) &&
        (item3rd.status == "down" ||
          item4th.status == "down" ||
          item5th.status == "down")
      ) {
        //item thu 2 có dấu hiệu tăng, item 3/4/5 giảm mạnh và current phải chạm đáy trong khoảng 90-95% -> trường hợp giảm liên tục, dò đáy
        //https://drive.google.com/file/d/1IvNFPNgg7BjUxbqN5BiRQ02ZglIvXMQD/view?usp=sharing
        let allowBuy = true;
        //B handle limitBuyPrice
        let addBuyType = "";
        if (data.limitBuyCoin == data.coin && data.limitBuyPrice > 0) {
          //case change coin
          if (data.currentPrice <= data.limitBuyPrice) {
            addBuyType = "_limitBuyPrice";
          } else {
            allowBuy = false;
          }
        } else {
          data.limitBuyCoin = data.coin;
          data.limitBuyPrice = 0;
        }
        //E handle limitBuyPrice
        if (allowBuy == true) {
          console.log(">>>>>> marketBuy");
          data.buyType = "ko_do_day" + addBuyType;
          order = await handleMarketBuy(item1st.close);
        }
      }
    } else {
      //progress == selling
      let dif = data.currentPrice - data.buyPrice;
      let difPc = (dif * 100) / data.buyPrice;
      data.difPc = difPc;
      console.log(">>>>>>>>> difPc", difPc);
      //B handle checkMa7SellDown
      let checkMa7SellDown = 1;
      if (config.checkMa7SellDown) {
        if (data.ma7OrderStt == "down" || data.ma7OrderStt == "normal") {
          checkMa7SellDown = 1;
        } else {
          checkMa7SellDown = 0;
        }
      }
      //E handle checkMa7SellDown
      let tpMin = data.tpMin;
      if (config.ruleNextBuy == "same" && data.waitHardBuy == false) {
        //waitHardBuy maybe will hard set tpMin
        if (data.timeLossTmp > 0) {
          tpMin = getNextPercentTP(tpMin, data.timeLossTmp);
        }
      }

      data.tpTimeLoss = tpMin;
      data.slPrice = data.buyPrice + (data.buyPrice / 100) * config.stopLoss; //stopLoss = -1.9 so will +
      data.tpPrice = data.buyPrice + (data.buyPrice / 100) * tpMin;
      if (data.tpQuick > 0 && tpMin > data.tpQuick) {
        tpMin = data.tpQuick; //after S, set data.tpQuick = 0
        data.usingTpQuick = true; //after S, set data.usingTpQuick = false
      }

      //if(difPc >= config.tpMin && data.currentPercentAffterBuy < data.tpMax){//dif 2% tp and currentPercent < 95%
      if (data.hardSell == true) {
        data.hardSell = false;
        data.sellType = "hard_sell";
        order = await handleMarketSell(data, item1st);
      } else if (difPc > 0) {
        //Check tpMode
        let allowSell = false;
        if (config.checkMa3Sell == true) {
          if (item1st.ma3 < item2nd.ma3) {
            allowSell = true;
            data.sellType = "checkMa3Sell ... ";
            order = await handleMarketSell(data, item1st);
          }
        }
        if (allowSell == false) {
          if (config.tpMode == "tpCandle") {
            //test set 3m
            if (difPc >= tpMin) {
              if (item2nd.status == "down" || item2nd.status == "normal") {
                data.sellType = "du_dinh tpCandle 2nd down";
                order = await handleMarketSell(data, item1st);
              } else {
                data.comment = "tpCandle item2nd up => keep ";
              }
            } else {
              data.comment = "back log case ma7OrderStt up";
            }
          } else if (difPc >= tpMin) {
            //config.tpMode == percent
            //dif 2%
            //TP
            //https://drive.google.com/file/d/1W0QWJQiGkfa6-S7fT7Dzkwfg-HhGqtsp/view?usp=sharing
            //https://drive.google.com/drive/folders/1n6qP25XFSPOTVvSLWn_aKIocDCpQHYx_?ths=true

            //TP
            if (
              item1st.change > config.saleOnTopAcc ||
              item2nd.change > config.saleOnTopAcc
            ) {
              //tp_up_on_top acc fast so tp when data.currentPrice <= tpMax
              //tp_up_on_top check realtime, back test not exact for test this case
              //fast keep
              //if (data.currentPrice <= data.chartTopHigh) {//95% this cond have problem
              if (
                data.currentPercentAffterBuy <= config.tpMax &&
                checkMa7SellDown
              ) {
                //<95%
                data.sellType = "tp_up_on_top checkMa7SellDown";
                order = await handleMarketSell(data, item1st);
              } else if (data.currentPercentAffterBuy <= config.tpMax) {
                // < 95%
                data.sellType = "tp_up_on_top";
                order = await handleMarketSell(data, item1st);
              } else if (
                data.currentPercentAffterBuy > config.tpMax &&
                data.timeLossTmp > 0
              ) {
                //https://drive.google.com/file/d/1jLdzHmeX_KrNQv3Tqru2hSV80ll_M9G7/view?usp=sharing
                // 95% < difPc < 100
                data.sellType =
                  "tp_up_on_top timeLossTmp tpTimeLoss " +
                  data.timeLossTmp +
                  " " +
                  data.tpTimeLoss;
                order = await handleMarketSell(data, item1st);
              } else {
                // > 95%
                //on top, keep it
              }
            } else {
              if (data.currentPercentAffterBuy > config.tpMax) {
                //on top, , keep it
              } else if (checkMa7SellDown) {
                //tp_up_not_fast acc slow so tp when reach tp setup

                data.sellType = "tp_up_not_fast checkMa7SellDown";
                order = await handleMarketSell(data, item1st);
              } else {
                //why this case not sell??
                //consider checkMa7SellDown
                data.sellType = "tp_up ma7OrderStt = up";
                order = await handleMarketSell(data, item1st);
              }
            }
          }
        }
      } else if (difPc <= config.stopLoss) {
        //stopLossTop
        //-2% => -3%
        //SL
        let allowSell = false;
        if (config.checkMa3Sell == true) {
          if (item1st.ma3 < item2nd.ma3) {
            allowSell = true;
            data.sellType = "checkMa3Sell ... ";
            order = await handleMarketSell(data, item1st);
          }
        }
        if (allowSell == false && difPc > config.stopLossBottom) {
          // not sell at case reduce to much

          data.sellType = "stop_loss";
          order = await handleMarketSell(data, item1st);
        }
      }
    }
  }
  console.log(">>>>>>> config", config);
  console.log(">>>>>>> data", data);
  console.log(">>>>>>> startItem", startItem);
  console.log(">>>>>>> onlyCheck: ", config.onlyCheck);
}

const { Console } = require("console");
const logTrade = new Console({
  stdout: fs.createWriteStream("./config/trade.log", {
    flags: "a",
  }),
});

async function saveData() {
  callBack = function (err) {
    if (err) throw err;
    console.log("saveData callBack completed");
  };
  fs.writeFile("./config/data.json", JSON.stringify(data), "utf8", callBack);
}
async function updateBuyData() {
  data.progress = "selling";
  data.limitBuyPrice = 0;
  data.limitBuyCoin = data.coin;
  saveData();
  console.log(">>>> updateBuyData");
}

async function updateSellData() {
  data.buyAmount = 0;
  data.progress = "buying";
  data.buyPrice = 0;
  data.affterBuyHighPrice = 0;
  data.affterBuyLowPrice = 0;
  data.buyType = "";
  data.tpQuick = 0;
  data.usingTpQuick = false;

  if (data.difPc > 0) {
    //TP
    data.tpAccelerate = level1m;
    let limitBuyPrice = await getLimitBuyPrice();
    console.log(">>>> limitBuyPrice", limitBuyPrice);
    data.limitBuyPrice = parseFloat(limitBuyPrice); //after buy remove it
    data.limitBuyCoin = data.coin; //after buy remove it
    if (config.checkMa7BuyUp == true) {
      data.flagMa7BuyUp = true;
    }
    data.countTp += 1;
  } else {
    data.countSl += 1;
  }
  if (data.stopAfterSell == true) {
    data.stopAfterSell = false;
    data.stopBot = true;
  }

  saveData();
  console.log(">>>> updateSellData");
}

async function printBalance(currentPrice) {
  const msg1 = `Balance: VCOIN: ${data.balance.VCOIN}, USDT: ${data.balance.USDT}`;
  data.totalUSDT = data.balance.VCOIN * currentPrice + data.balance.USDT;
  const msg2 = `Total USDT: ${data.totalUSDT}. \n`;
  console.log(msg1);
  console.log(msg2);

  logTrade.log(msg1);
  logTrade.log(msg2);
  logTrade.log("-----------");
}

function getQuantityBuy(lastPrice) {
  let quantity = config.TRADE_SIZE / lastPrice;
  //B check ruleNextBuy
  if (config.ruleNextBuy == "add_more") {
    let timeLoss = config.slContinuous - data.slContinuous;
    quantity = (config.TRADE_SIZE * (timeLoss + 1)) / lastPrice;
  }
  //E check ruleNextBuy
  return quantity;
}
async function marketBuy(lastPrice) {
  const quantity = getQuantityBuy(lastPrice);

  const order = await binance.createMarketOrder(pairs, "buy", quantity);
  data.affterBuyHighPrice = order.price;
  data.affterBuyLowPrice = order.price;
  data.sellType = "";
  data.buyPrice = order.price;

  const balance = await binance.fetchBalance();
  let coin = data.coin.toUpperCase();
  data.balance.VCOIN = balance.total[coin];
  data.balance.USDT = balance.total["USDT"];
  //after mua 2nd,3rd will add more
  data.buyAmount = data.balance.VCOIN;

  return order;
}
async function marketSell() {
  console.log(">>>>>> marketSell");
  const quantity = data.buyAmount;
  const order = await binance.createMarketOrder(pairs, "sell", quantity);
  //B lock item
  lockItemsTime = [];
  lockItemsTime.push(item1st.timestamp, item2nd.timestamp, item3rd.timestamp);
  //E lock item
  if (data.sellType == "stop_loss") {
    reduceSlContinuous();
  } else {
    //TP
    data.maxTriggerLoss = config.maxTriggerLoss;
    initSlContinuous();
  }
  return order;
}
async function getLimitBuyPrice() {
  let prices = [];
  if (item1st.status == "up") {
    prices.push(item1st);
  }
  if (item2nd.status == "up") {
    prices.push(item2nd);
  }
  if (item3rd.status == "up") {
    prices.push(item3rd);
  }
  if (item4th.status == "up") {
    prices.push(item4th);
  }
  if (item5th.status == "up") {
    prices.push(item5th);
  }
  if (item6th.status == "up") {
    prices.push(item6th);
  }
  if (item7th.status == "up") {
    prices.push(item7th);
  }

  let hlPrices = await OHLCVformatHightLow(prices);

  let min = Math.min.apply(null, hlPrices);
  let max = Math.max.apply(null, hlPrices);
  return min;
}
async function handleMarketBuy(lastPrice) {
  let order = false;
  if (config.real === true) {
    if (config.slContinuousTmpOn == true) {
      let halfQuantity = config.TRADE_SIZE / 2 / data.currentPrice;
      if (
        data.timeLossTmp > 0 &&
        data.buyAmount >= halfQuantity &&
        data.currentPrice >= data.slPrice
      ) {
        //check case increase back
        data.timeLossTmp = data.timeLossTmp - 1;
        data.slContinuousTmp = data.slContinuousTmp + 1;
        data.progress == "selling";
      } else {
        //normal buy
        order = await marketBuy(lastPrice);
      }
    } else {
      order = await marketBuy(lastPrice);
    }
  } else {
    order = await marketBuyLocal(lastPrice);
  }
  if (order) {
    logTrade.log(">>>>> data ", data);
    logTrade.log(">>>>> order", order);
    logTrade.log(">>>>> item1st", item1st);
    printBalance(item1st.close);
    updateBuyData();
  }
  return order;
}
async function marketBuyLocal(lastPrice) {
  const quantity = config.TRADE_SIZE / lastPrice;
  const order = await createMarketOrderLocal(pairs, "buy", quantity);
  return order;
}
async function createMarketOrderLocal(pairs, mode, quantity) {
  let order = {
    timestamp: item1st.timestamp,
    datetime: item1st.time,
    symbol: pairs,
    type: "market",
    timeInForce: "GTC",
    side: mode,
    price: data.currentPrice,
    amount: quantity,
    cost: data.currentPrice * quantity,
    average: 43394.61,
    filled: 0.002309,
    remaining: 0,
    status: "closed",
  };
  if (mode == "buy") {
    //nếu đủ tiền mới mua
    if (data.balance.USDT > config.TRADE_SIZE) {
      data.balance.USDT = data.balance.USDT - config.TRADE_SIZE;
      data.balance.VCOIN = data.balance.VCOIN + order.amount;
      data.affterBuyHighPrice = order.price;
      data.affterBuyLowPrice = order.price;
      data.sellType = "";
      data.buyAmount = order.amount;
      data.buyPrice = order.price;
    } else {
      logTrade.log("-----------");
      logTrade.log("Not enough USDT");
      return false;
    }
  } else {
    data.balance.USDT = data.balance.USDT + order.cost;
    data.balance.VCOIN = data.balance.VCOIN - order.amount;
    data.buyType = "";
  }

  return order;
}

async function handleMarketSell(data, item1st) {
  console.log(">>>>> handleMarketSell", data);
  let allowSell = true;
  if (config.slContinuousTmpOn == true && data.sellType == "stop_loss") {
    allowSell = reduceSlContinuousTmp();
  }

  let order = false;
  if (allowSell == true) {
    if (config.real == true) {
      order = await marketSell();
    } else {
      order = await marketSellLocal();
    }
    data.slContinuousTmp = config.slContinuousTmp;
    data.timeLossTmp = 0;

    logTrade.log(">>>>> data ", data);
    logTrade.log(">>>>> order", order);
    logTrade.log(">>>>> item1st", item1st);
    if (data.difPc > 0) {
      logTrade.log("TP difPc: ", data.difPc);
    } else {
      logTrade.log("stopLoss difPc: ", data.difPc);
    }
    printBalance(item1st.close);
    updateSellData(); //save data too
  } else {
    //allow to buy one more
    saveData();
  }

  return order;
}
async function marketSellLocal() {
  console.log(">>>>>> marketSellLocal");
  const quantity = data.buyAmount;
  const order = await createMarketOrderLocal(pairs, "sell", quantity);
  //B lock item
  lockItemsTime = [];
  lockItemsTime.push(item1st.timestamp, item2nd.timestamp, item3rd.timestamp);
  //E lock item
  if (data.sellType == "stop_loss") {
    reduceSlContinuous();
  } else {
    initSlContinuous();
  }
  return order;
}
async function analysisAccelerate(prices) {
  let hlPrices = await OHLCVformatHightLow(prices);

  let min = Math.min.apply(null, hlPrices);
  let max = Math.max.apply(null, hlPrices);

  let dif = max - min;

  let level = 0;
  if (dif > 0) {
    level = (dif * 100) / min;
  } else if (dif < 0) {
    level = (dif * 100) / max;
  }
  return level;
}

async function OHLCVformatHightLow(prices) {
  const hlPrices = prices.reduce((acc, price) => {
    acc.push(price.high);
    acc.push(price.low);
    return acc;
  }, []);
  return hlPrices;
}
async function loopLoop() {
  stopBot = checkStopBot();
  if (stopBot == false) {
    await mainHandle();
    startItem += 1;
    data.acc = 0;
    if (data.difPc > 0) {
      data.acc = (data.difPc * 100) / data.tpTimeLoss;
    }
    if (config.tpMode == "percent") {
      if (
        data.hardBuy == true ||
        data.hardSell == true ||
        data.stopBot == true
      ) {
        await dalay(1 * 1000); //1s
      } else if (data.currentPercentAffterBuy >= config.tpMax) {
        await dalay(1 * 1000); //1s
      } else if (item1st.accelerate >= 3 && data.progress == "selling") {
        //Accelerate high
        await dalay(1 * 1000); //1s
      } else if (data.acc >= 80) {
        await dalay(2 * 1000); //2s
      } else {
        await dalay(3 * 1000); //4s
      }
    } else {
      if (
        data.hardBuy == true ||
        data.hardSell == true ||
        data.stopBot == true
      ) {
        await dalay(1 * 1000); //1s
      } else {
        await dalay(4 * 1000); //4s
      }
    }

    loopLoop();
  }
}

async function loopSolf() {
  //update gia tốc, currentPrice item1st mỗi 5s, để đảm bảo trong trường hợp giá tăng giảm mạnh có thể control
  //if != time tức là đã có nến mới, call mainHandle() if lastcall > 10s
}

//node func/tradeReal.js
async function main() {
  await initTopBot();
  //mainHandle()
  await loopLoop();
  //await dalay(5* 1000);
}
main();

//check buy and sell
