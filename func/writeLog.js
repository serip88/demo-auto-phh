var fs = require('fs');
var util = require('util');

var {appDir} = require('../main');


var log_file = fs.createWriteStream(appDir + '/debug.log', {flags : 'w'});
var log_stdout = process.stdout;

console.log = function(d) { //
  log_file.write(util.format(d) + '\n');
  log_stdout.write(util.format(d) + '\n');
};
console.log(appDir)

//node func/writeLog.js