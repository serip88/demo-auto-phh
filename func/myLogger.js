// get the Console class
const { Console } = require("console");

// get fs module for creating write streams
const fs = require("fs");

var {appDir} = require('../main');

// make a new logger
const myLogger = new Console({
  stdout: fs.createWriteStream(appDir + "/debug.log", {flags : 'a'}),
  stderr: fs.createWriteStream(appDir + "/errStdErr.log"),
});

// saving to normalStdout.txt file
myLogger.log("Hello 😃. This will be saved in debug.txt file");

// saving to errStdErr.txt file
myLogger.error("Its an error ❌. This will be saved in errStdErr.log file");



//node func/myLogger.js