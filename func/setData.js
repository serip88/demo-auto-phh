//let data = require("../config/data.json");
let dataTrigger = require("../func/checkTriggerBuy/data.json");
const fs = require("fs");
const ccxt = require("ccxt");
let { appDir, privateConfig } = require("../main");
const binance = new ccxt.binance({
  apiKey: privateConfig.API_R,
  secret: privateConfig.SECRET_R,
});
//binance.setSandboxMode(true);

const myArgs = process.argv.slice(2);

function checkCoinValid(balance, coin) {
  if (typeof balance.total[coin] != "undefined") {
    return true;
  } else {
    return false;
  }
}

async function main() {
  //const balance = await binance.fetchBalance();
  //tạm thời bỏ vì timeout cao

  if (myArgs.length == 0) {
    console.log("Nothing to update >>>");
    return;
  }
  var app = "default";
  var path = "./config/data.json";
  if (myArgs.length == 2) {
    let item = myArgs[1];
    let keyValues = item.split("=");
    let key = "";
    let value = "";
    if (keyValues.length == 2) {
      key = keyValues[0];
      value = keyValues[1];
      if (key == "app") {
        app = value;
        path = "./config/" + app + "/data.json";
      }
    }
  }
  fs.readFile(path, "utf8", function (err, dataEncode) {
    let data = JSON.parse(dataEncode);
    let isUpdate = false;
    let isUpdateTrigger = false;

    myArgs?.map((item, index) => {
      let keyValues = item.split("=");
      let key = "";
      let value = "";
      if (keyValues.length == 2) {
        key = keyValues[0];
        value = keyValues[1];
      }
      switch (key) {
        case "hard_buy_coin":
          //hard_buy_coin=status/coin
          //hard_buy_coin=1/beta
          cmdArr = value.split("/");
          if (cmdArr.length == 2) {
            let hardBuy = cmdArr[0] == 1 ? true : false;
            let coin = cmdArr[1].toUpperCase();
            //let coinValid = checkCoinValid(balance, coin);
            let coinValid = true;
            if (coinValid == false) {
              console.log("Sorry, we don't have this coin: ", coin);
              return;
            }
            if (data.progress == "buying") {
              //if (1) {
              data.hardBuy = hardBuy;
              data.coin = coin;
              isUpdate = true;
            } else {
              console.log("Sorry, cannot set when selling: ", data.coin);
              return;
            }
          }
          break;
        case "waitHardBuy":
        case "stopBot":
        case "hardSell":
        case "tpQuick":
        case "tpMin":
        case "hardBuy":
        case "coin":
        case "stopAfterSell":
          cmdArr = value.split("/");
          if (cmdArr.length == 1) {
            if (key == "hardSell") {
              if (data.progress == "selling") {
                let val = cmdArr[0] == "true" ? true : false;
                data[key] = val;
                isUpdate = true;
                console.log(">>>>>>>>>> data", data, val, cmdArr);
              }
            } else if (key == "tpQuick" || key == "tpMin") {
              let val = parseFloat(cmdArr[0]);
              if (val >= 0) {
                data[key] = val;
                isUpdate = true;
              }
            } else if (key == "coin") {
              if (data.progress == "buying") {
                let val = cmdArr[0];
                data[key] = val.toUpperCase();
                isUpdate = true;
              }
            } else if (key == "hardBuy") {
              if (data.progress == "buying") {
                let val = cmdArr[0] == "true" ? true : false;
                data[key] = val;
                isUpdate = true;
              }
            } else {
              let val = cmdArr[0] == "true" ? true : false;
              data[key] = val;
              isUpdate = true;
              console.log(">>>>>>>>>> data", data, val, cmdArr);
            }
          }
          break;
        case "app":
          app = value;
          break;
        case "stopTrigger":
          cmdArr = value.split("/");
          if (cmdArr.length == 1) {
            if (key == "stopTrigger") {
              let val = cmdArr[0] == "true" ? true : false;
              dataTrigger["stopBot"] = val;
              isUpdateTrigger = true;
              console.log(">>>>>>>>>> data", dataTrigger, val, cmdArr);
            }
          }
          break;
        default:
          console.log("Sorry, that is not something I know how to do.");
      }
    });

    // if (data.hardBuy_cmt != dataDecode.hardBuy_cmt) {
    //   data.hardBuy_cmt = dataDecode.hardBuy_cmt;
    //   isUpdate = true;
    // }
    if (isUpdate == true) {
      callBack = function (err) {
        if (err) throw err;
        console.log("update data.json callBack completed");
      };
      //tạm khóa
      if (app == "default") {
        fs.writeFile(
          "./config/data.json",
          JSON.stringify(data),
          "utf8",
          callBack
        );
      } else {
        fs.writeFile(
          "./config/" + app + "/data.json",
          JSON.stringify(data),
          "utf8",
          callBack
        );
      }
      console.log("Done >>>", data);
    } else {
      console.log("Nothing data to update >>>");
    }

    if (isUpdateTrigger == true) {
      callBack = function (err) {
        if (err) throw err;
        console.log("update trigger data.json callBack completed");
      };
      //tạm khóa
      fs.writeFile(
        "./func/checkTriggerBuy/data.json",
        JSON.stringify(dataTrigger),
        "utf8",
        callBack
      );
      console.log("Done >>>", dataTrigger);
    } else {
      console.log("Nothing dataTrigger to update >>>");
    }
  });
}
main();
//Hàm hard set data
//node func/setData.js
//node func/setData.js hard_buy_coin=1/beta
//node func/setData.js hard_buy_coin=1/flm
//node func/setData.js hard_buy_coin=1/shib
//node func/setData.js waitHardBuy=true
//node func/setData.js stopBot=true
//node func/setData.js hardSell=true
//node func/setData.js stopAfterSell=true
//node func/setData.js tpQuick=0.5
//node func/setData.js coin=eth
//node func/setData.js hardBuy=true
//node func/setData.js stopTrigger=true
//node func/setData.js hardSell=true app=candle
//node func/setData.js stopBot=true app=candle
//node func/setData.js coin=shib app=candle
//node func/setData.js tpMin=4 app=candleV2
