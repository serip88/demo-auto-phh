const ccxt = require("ccxt");
const moment = require("moment");
let { appDir, privateConfig } = require("../main");

let apiSec = {
  apiKey: privateConfig.API_R,
  secret: privateConfig.SECRET_R,
};
const binance = new ccxt.binance(apiSec);
//binance.setSandboxMode(true);

async function fetch() {
  const balance = await binance.fetchBalance();
  console.log(balance.total);
}
//get total balance

function getNextPercentTP() {
  let tpMin = 2;
  let tpMin1 = (tpMin / 2) * 0 + tpMin;
  let tpMin2 = (tpMin / 2) * 1 + tpMin;
  let tpMin3 = (tpMin / 2) * 2 + tpMin;
  let tpMin4 = (tpMin / 2) * 3 + tpMin;

  console.log("<<<<<<<<<<< getNextPercentTP", tpMin1, tpMin2, tpMin3, tpMin4);
}
async function test() {
  let fPrices = [];
  fPrices.push({ status: "up" });
  fPrices.push({ status: "down" });
  fPrices.push({ status: "down" });
  fPrices.push({ status: "down" });
  fPrices.push({ status: "down" });
  fPrices.push({ status: "down" });
  fPrices.push({ status: "down" });
  checkBuy(fPrices);
}
function checkBuy(fPrices) {
  let itemDown = 0;
  fPrices.forEach((item, index) => {
    if (item.status == "down") {
      itemDown += 1;
    }
  });

  let checkBuy = {};
  checkBuy.down6Time = false;
  if (itemDown >= 6) {
    checkBuy.down6Time = true;
  }

  console.log(checkBuy);
  return checkBuy;
}
function tpSLDay() {
  let d1 = new Date();
  console.log(d1.toISOString().split("T")[0]);
  let d2 = new Date(d1);
  d2.setDate(d2.getDate() + 1);
  let tpSlDay = {};
  tpSlDay.tpDay = 1;
  tpSlDay.slDay = 2;
  tpSlDay.startToday = 4174;
  tpSlDay.pot = 900;
  tpSlDay.potTP = (tpSlDay.pot / 100) * (100 + tpSlDay.tpDay);
  tpSlDay.potSL = (tpSlDay.pot / 100) * (100 - tpSlDay.slDay);
  tpSlDay.tpDayAmount = tpSlDay.startToday - tpSlDay.pot + tpSlDay.potTP;
  tpSlDay.slDayAmount = tpSlDay.startToday - tpSlDay.pot + tpSlDay.potSL;
  tpSlDay.today = d1.toISOString().split("T")[0];
  tpSlDay.nextDay = d2.toISOString().split("T")[0];

  return tpSlDay;
}
function lamDuongSoAm() {
  Math.abs(-1);

  let comment_debug = [];
  comment_debug.push(["code_log", 100]);
  comment_debug.push(["code_log", 101]);
  console.log(">>>>> comment_debug", Math.abs(-1));
}
function checkAllowBuy() {
  let newestActionTime = "2022-07-11T20:48:00+07:00";
  let timeAllowBuy = new Date(newestActionTime);
  timeAllowBuy.setMinutes(timeAllowBuy.getMinutes() + 1);
  console.log(">>>>", timeAllowBuy);
}
function compareTime() {
  let d1 = new Date("2022-08-10T10:15:00-04:00");
  let d2 = new Date("2022-08-10T10:16:00-04:00");
  if (d2.getTime() > d1.getTime()) {
    console.log("2 lớn hơn");
  }
}

function addMinutes() {
  let d1 = new Date("2022-08-10T10:15:00-04:00");
  let d2 = new Date("2022-08-10T10:15:00-04:00");

  d2.setMinutes(d2.getMinutes() + 1);
  console.log(d1);
  console.log(d2);
}
function arrCheckKeyExist(arr, key) {
  if (typeof arr[key] == "undefined") {
    return false;
  } else {
    return true;
  }
}

async function OHLCVformatHightLow(prices) {
  const hlPrices = prices.reduce((acc, price) => {
    acc.push(price.high);
    acc.push(price.low);
    return acc;
  }, []);
  return hlPrices;
}

function currentTime() {
  var today = new Date();
  let locDate = today.toLocaleDateString();
  let timeString = today.toTimeString().split(" ")[0];
  let locDateString = [locDate, timeString].join(" ");
  let item = { time: locDateString, price: 0.096 };
  let items = [item, item];
  console.log(">>>>>>>>>>> currentTime: ", items);
}

//node func/fetch.js
test();
//fetch();
//currentTime();
//getNextPercentTP();
