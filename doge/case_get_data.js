const ccxt = require("ccxt");
const moment = require("moment");
const dalay = require('delay');

var {config} = require('../main');

const binance = new ccxt.binance({
  apiKey: config.API_R,
  secret: config.SECRET_R
});
//binance.setSandboxMode(true)


async function getLastPrice(){

  const prices = await binance.fetchOHLCV('DOGE/USDT', '1m', undefined, 1);
  
  const lastPrice = prices[0][4]
  console.log(lastPrice );
}

async function loopLoop(){
  while(true){
    await getLastPrice();
    await dalay(30* 1000);
  }
}
//node doge/case_get_data.js | tee -a doge/log/doge_log.txt
loopLoop()