const chart = require('asciichart')
const fs = require('fs')
const dalay = require('delay');

//get data từ a_log.txt

function plotAsset(){
  const lines = fs.readFileSync('./doge/log/doge_log.txt', 'utf8').split('\n')
  const assets = []

  const start = lines.length - 192
  lines.forEach( (line, key) => {
    if( key > start){
      var asset = line.trim()
      if(asset){
        assets.push(parseFloat(asset))
      }
    }
  });

  console.clear()
  //console.log(assets)
  var format = function (x, i) { var padding = '       '; return (padding + x.toFixed (4)).slice (-padding.length) }
  console.log(chart.plot(assets, {
    height: 50,
    format: format
  }))
}

//node chart2.js
async function loopLoop(){
  while(true){
    await plotAsset();
    await dalay(10* 1000);
  }
}
//node doge/case_init_chart.js
loopLoop()
//plotAsset()

