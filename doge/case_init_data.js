//xử lý in ghi gia tốc vào file a.txt để theo dõi đồ thị cho dễ nếu số lượng vượt quá 200 dòng thì xóa đi ghi lại
//-> nó là file log xử lý tạm thời, ko xóa đi nếu đang giao động mạnh



const ccxt = require("ccxt");
const moment = require("moment");
const dalay = require('delay');

var {config} = require('../main');

const binance = new ccxt.binance({
  apiKey: config.API_R,
  secret: config.SECRET_R
});
//binance.setSandboxMode(true)


async function getLastPrice(){

  const prices = await binance.fetchOHLCV('DOGE/USDT', '1m', undefined, 200);
  let lastPrice = 0

  const bPrices = prices.map(price => {
    lastPrice = price[4]
    console.log(lastPrice );
    return {
      timestamp: moment(price[0]).format(),
      open: price[1],
      high: price[2],
      low: price[3],
      close: price[4],
      volume: price[5]
    }
    //    console.log(JSON.stringify(all) );
  })

}

//node doge/case_init_data.js | tee -a doge/log/doge_log.txt
getLastPrice()