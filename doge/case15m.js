//đây sẽ xử lý giảm 3 lần liên tiếp sẽ mua nếu gia tốc đạt yêu cầu


const ccxt = require("ccxt");
const moment = require("moment");
const dalay = require('delay');

var {config} = require('../main');

const binance = new ccxt.binance({
  apiKey: config.API_R,
  secret: config.SECRET_R
});
//binance.setSandboxMode(true)

let level1m = 0;
let level2m = 0;
let level3m = 0;
let level4m = 0;
let level5m = 0;


var itemNewest = false;
var item2nd = false;
var item3rd = false;
var item4th = false;
var item5th = false;
var item6th = false;
var progress = false;

async function mainHandle(){
  await initItems();
  await checkBuy();
}

async function initItems(){
  let prices = await binance.fetchOHLCV('DOGE/USDT', '15m', undefined, 6);

  itemNewest = prices[5];
  item2nd = prices[4];
  item3rd = prices[3];
  item4th = prices[2];
  item5th = prices[1];
  item6th = prices[0];
}
async function checkBuy(){

  let pricesPure = await binance.fetchOHLCV('DOGE/USDT', '15m', undefined, 6);
  console.log('prices prices prices prices', pricesPure)
  let price = pricesPure[5];
  if(price[0] != itemNewest[0]){
    console.log('time khac nhau >>>', price[0])
    await initItems();
  }

  let prices = []
  
  prices[0]= itemNewest
  prices[1]= item2nd
  //level1m = await analysisAccelerate(prices);
  level1m = await analysisAccelerate2(prices);

  prices = []
  prices[0]= itemNewest
  prices[1]= item3rd
  //level2m = await analysisAccelerate(prices);
  level2m = await analysisAccelerate2(prices);

  prices = []
  prices[0]= itemNewest
  prices[1]= item4th
  //level3m = await analysisAccelerate(prices);
  level3m = await analysisAccelerate2(prices);

  prices = []
  prices[0]= itemNewest
  prices[1]= item5th
  //level4m = await analysisAccelerate(prices);
  level4m = await analysisAccelerate2(prices);

  prices = []
  prices[0]= itemNewest
  prices[1]= item6th
  //level5m = await analysisAccelerate(prices);
  level5m = await analysisAccelerate2(prices);

  console.log('level 1 >>>>', level1m)
  console.log('level 2 >>>>', level2m)
  console.log('level 3 >>>>', level3m)
  console.log('level 4 >>>>', level4m)
  console.log('level 5 >>>>', level5m)

  let format = []
  pricesPure.forEach( (line, key) => {
    let stt = "normal";
    if(key && typeof(pricesPure[key-1]) != undefined ){
      let pre = pricesPure[key-1];
      if(line[4] > pre[4]){
        stt = "up";
      }else if(line[4] < pre[4]){
        stt = "down";
      }
      
    }
    line.push(stt)
    line.push(moment(line[0]).format())
    
    format.push(line)
  });

  
  console.log('format >>>>>>>>>>>', format)
  if(level2m > 1.5){
    //so sánh giá high và close
    let dif = price[2] - price[4];
    if(dif <= 0.0002 ){//buy
      console.log("marketBuy marketBuy marketBuy marketBuy")
      //order = marketBuy()
      progress = 'bought'
    }
  }
}

async function marketBuy(){
  const order = await binance.createMarketOrder('BTC/USDT', 'buy', 100)
  return order
}
async function analysisAccelerate2(prices){
  
  var hlPrices = await OHLCVformatHightLow(prices)

  var min = Math.min.apply(null, hlPrices)
  var max = Math.max.apply(null, hlPrices)

  let dif = max - min;

  let level = 0
  if(dif > 0){
    level = (dif*100)/min
  }else if(dif < 0){
    level = (dif*100)/max
  }
  return level
}


async function OHLCVformatHightLow(prices){//2,3

  const hlPrices = prices.reduce((acc, price)  => {
    acc.push(price[2])
    acc.push(price[3])
    return acc
  }, [])
  return hlPrices
}

async function main(){
  const prices = await binance.fetchOHLCV('DOGE/USDT', '1m', undefined, 1);
  const bPrices = prices.map(price => {
    return {
      timestamp: moment(price[0]).format(),
      open: price[1],
      high: price[2],
      low: price[3],
      close: price[4],
      volume: price[5]
    }
  })
  console.log(bPrices);
}

async function printBalance(){
  const balance = await binance.fetchBalance();
  console.log(balance)
}



//printBalance()
//node doge/case15m.js
mainHandle()
//main()


