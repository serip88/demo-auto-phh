const ccxt = require("ccxt");
const moment = require("moment");
const delay = require('delay');
let status = 'normal';
let level1m = 0;
let level2m = 0;
let level3m = 0;
let level4m = 0;
let level5m = 0;

const binance = new ccxt.binance({
  apiKey: 'YAlGLKtCOsQybLB9vZ59vxmHJ17JswdTU2qw00HY3Y3Ks7cGqQUDxcPy0OCeltQl',
  secret: 'WTyHqNygotrAYmKRZa0iWmx7M2KkTTSowanjH8jeFdQTv2aLTOyWLeer2rv9UzkM'
});
binance.setSandboxMode(true)



async function printBalance(){
  const balance = await binance.fetchBalance();
  console.log(balance)
}

async function printBalance2(btcPrice){
  const balance = await binance.fetchBalance();
  const total = balance.total
  console.log(`Balance: BTC ${total.BTC}, USDT: ${total.USDT}`);
  console.log(`Total USDT: ${(total.BTC - 1) * btcPrice + total.USDT}. \n`);

}

async function getPrice(pair){
  pair = 'BTC/USDT'
}

async function main(){
  

  const prices = await binance.fetchOHLCV('BTC/USDT', '1m', undefined, 3);
  const bPrices = prices.map(price => {
    return {
      timestamp: moment(price[0]).format(),
      open: price[1],
      high: price[2],
      low: price[3],
      close: price[4],
      volume: price[5]
    }
  })
  console.log(prices);
  console.log(bPrices);
}

/* fetchOHLCV
'timeframes': {                      // empty if the exchange.has['fetchOHLCV'] !== true
    '1m': '1minute',
    '1h': '1hour',
    '1d': '1day',
    '1M': '1month',
    '1y': '1year',
}*/
async function accelerate(){
  const prices = await binance.fetchOHLCV('BTC/USDT', '1m', undefined, 2);
  const bPrices = prices.map(price => {
    return {
      timestamp: moment(price[0]).format(),
      open: price[1],
      high: price[2],
      low: price[3],
      close: price[4],
      volume: price[5]
    }
  })
  //console.log(bPrices);
  let fakeData = []
  fakeData.push([ 1636656300000, 64664.33, 64726.04, 64268.44, 64668.87, 1.330681 ])
  fakeData.push([ 1636656360000, 64723.84, 64795.95, 64723.52, 64723.83, 0.316313 ])

  let newPrice = fakeData
  

  let dif = newPrice[1][4] - newPrice[0][4];

  let min = newPrice[1][4];
  let max = newPrice[0][4];
  let level = 0;

  if(newPrice[1][4] > newPrice[0][4]){
    max = newPrice[1][4];
    min = newPrice[0][4]
  }else if(newPrice[1][4] < newPrice[0][4]){
    min = newPrice[1][4];
    max = newPrice[0][4]
  }

  
  if(dif > 0){
    status = 'push'
    level = (dif*100)/min
  }else if(dif < 0){
    status = 'pull'
    level = (dif*100)/max
  }else{
    status = 'normal'
  }
  console.log(status, dif, level)
}

async function accelerate1m(){
  let prices = await binance.fetchOHLCV('BTC/USDT', '1m', undefined, 2);

  console.log('prices >>' , prices)

  // prices = [] //fake data
  // prices.push([ 1636656300000, 64664.33, 64726.04, 64268.44, 60525.02, 1.330681 ])
  // prices.push([ 1636656360000, 64723.84, 64795.95, 64723.52, 60890.39, 0.316313 ])
  
  let level = await analysisAccelerate(prices);

  console.log('1m', level)
}

async function analysisAccelerate2(prices){
  
  var hlPrices = await OHLCVformatHightLow(prices)

  var min = Math.min.apply(null, hlPrices)
  var max = Math.max.apply(null, hlPrices)

  let dif = max - min;

  let level = 0
  if(dif > 0){
    level = (dif*100)/min
  }else if(dif < 0){
    level = (dif*100)/max
  }
  return level
}
async function analysisAccelerate(prices){
  
  //get close price
  var closePrices = await OHLCVformatClose(prices)

  var min = Math.min.apply(null, closePrices)
  var max = Math.max.apply(null, closePrices)
  var length = prices.length - 1

  let dif = prices[length][4] - prices[0][4];
  
  let level = 0
  if(dif > 0){
    status = 'push'
    level = (dif*100)/min
  }else if(dif < 0){
    status = 'pull'
    level = (dif*100)/max
  }else{
    status = 'normal'
  }
  //console.log(status, dif, level)
  return level
}

async function OHLCVformatClose(prices){

  const closePrices = prices.map(price => {
    return price[4]
  })
  return closePrices
}

async function OHLCVformatHightLow(prices){//2,3

  const hlPrices = prices.reduce((acc, price)  => {
    acc.push(price[2])
    acc.push(price[3])
    return acc
  }, [])
  return hlPrices
}
var itemNewest = false;
var item2nd = false;
var item3rd = false;
var item4th = false;
var item5th = false;
var item6th = false;
var progress = false;

async function mainHandle(){
  await initItems();
  await checkBuy();
}

async function initItems(){
  let prices = await binance.fetchOHLCV('BTC/USDT', '1m', undefined, 6);

  itemNewest = prices[5];
  item2nd = prices[4];
  item3rd = prices[3];
  item4th = prices[2];
  item5th = prices[1];
  item6th = prices[0];

}
async function checkBuy(){

  let pricesPure = await binance.fetchOHLCV('BTC/USDT', '1m', undefined, 6);
  let price = pricesPure[5];
  if(price[0] != itemNewest[0]){
    console.log('time khac nhau >>>', price[0])
    await initItems();
  }

  let prices = []
  
  prices[0]= itemNewest
  prices[1]= item2nd
  //level1m = await analysisAccelerate(prices);
  level1m = await analysisAccelerate2(prices);

  prices = []
  prices[0]= itemNewest
  prices[1]= item3rd
  //level2m = await analysisAccelerate(prices);
  level2m = await analysisAccelerate2(prices);

  prices = []
  prices[0]= itemNewest
  prices[1]= item4th
  //level3m = await analysisAccelerate(prices);
  level3m = await analysisAccelerate2(prices);

  prices = []
  prices[0]= itemNewest
  prices[1]= item5th
  //level4m = await analysisAccelerate(prices);
  level4m = await analysisAccelerate2(prices);

  prices = []
  prices[0]= itemNewest
  prices[1]= item6th
  //level5m = await analysisAccelerate(prices);
  level5m = await analysisAccelerate2(prices);

  console.log('level 1 >>>>', level1m)
  console.log('level 2 >>>>', level2m)
  console.log('level 3 >>>>', level3m)
  console.log('level 4 >>>>', level4m)
  console.log('level 5 >>>>', level5m)


  let format = []
  pricesPure.forEach( (line, key) => {
    let stt = "normal";
    if(key && typeof(pricesPure[key-1]) != undefined ){
      let pre = pricesPure[key-1];
      if(line[4] > pre[4]){
        stt = "up";
      }else if(line[4] < pre[4]){
        stt = "down";
      }
      
    }
    line.push(stt)
    format.push(line)
  });

  console.log('prices prices prices prices', pricesPure)
  console.log('format >><M><><><>', format)
  if(level2m > 0.2){
    //so sánh giá high và close
    let dif = price[2] - price[4];
    if(dif <= 0.0002 ){//buy
      order = marketBuy()
      progress = 'bought'
    }
  }
}

async function checkSell(order){

  

}

async function stopLoss(order){

}
async function takeProfit(order){

}
async function marketBuy(){
  const order = await binance.createMarketOrder('BTC/USDT', 'buy', 100)
  return order
}
async function marketSell(){
  const order = await binance.createMarketOrder('BTC/USDT', 'sell', 100)
}
//node formula.js
// check level 5 phut
mainHandle()
//checkBuy()
//accelerate1m()
//main()
//printBalance()


