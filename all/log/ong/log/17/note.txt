- SL: 5
- TP: 6
"totalUSDT":10805.81217418042

issue #16
try case sell TP if ma7OrderStt = down
ma7OrderStt = up -> ignore
config.checkMa7SellDown = 1
apply sell type :tp_up_on_top and tp_up_not_fas
https://drive.google.com/file/d/12tHiYrEeRRzNtgtlHqnQSoxz0TcHpjxb/view?usp=sharing



{"progress":"selling","progress_cmt":"buying/selling","currentPrice":0.7629,"currentChart":"normal","currentChart_cmt":"normal/up/down","affterBuyHighPrice":0.7629,"affterBuyLowPrice":0.7629,"currentPercentAffterBuy":43.83954154727783,"priceSL":0,"buyType":"du_dinh_co_node_giam","buyType_cmt":"du_dinh/du_dinh_volume/du_dinh_volume_applitude/do_day/ko_do_day/du_dinh_co_node_giam","sellType":"","sellType_cmt":"tp_up_on_top/tp_up_not_fast/tp_down/stop_loss","ma7":0.7614428571428571,"ma7OrderStt":"up","chartTop":0.834,"chartBottom":0.5732,"chartTopHigh":0.8209599999999999,"chartTopLow":0.80792,"chartBottomHigh":0.59928,"chartBottomLow":0.5862400000000001,"balance":{"USDT":7805.812174180421,"VCOIN":3932.3633503735746},"buyAmount":3932.3633503735746,"buyPrice":0.7629,"totalUSDT":10805.81217418042}
{
  "TRADE_SIZE": 3000,
  "pairs": "ONG/USDT",
  "tpMin": 2,
  "tpMax": 95,
  "stopLoss": -2,
  "saleOnTopAcc": 1,
  "checkMa7BuyUp": 1,
  "checkMa7SellDown": 1
}

backlog 0:  timestamp: '2022-01-22T12:24:00+07:00',
-> mua cao qua

backlog 1: mua bán lỗ xem lại

-----------
>>>>> data  {
  progress: 'buying',
  progress_cmt: 'buying/selling',
  currentPrice: 0.66,
  currentChart: 'normal',
  currentChart_cmt: 'normal/up/down',
  affterBuyHighPrice: 0.66,
  affterBuyLowPrice: 0.66,
  currentPercentAffterBuy: 82.45125348189394,
  priceSL: 0,
  buyType: 'du_dinh_volume',
  buyType_cmt: 'du_dinh/du_dinh_volume/du_dinh_volume_applitude/do_day/ko_do_day/du_dinh_co_node_giam',
  sellType: '',
  sellType_cmt: 'tp_up_on_top/tp_up_not_fast/tp_down/stop_loss',
  ma7: 0.6567428571428572,
  ma7OrderStt: 'up',
  chartTop: 0.834,
  chartBottom: 0.5732,
  chartTopHigh: 0.8209599999999999,
  chartTopLow: 0.80792,
  chartBottomHigh: 0.59928,
  chartBottomLow: 0.5862400000000001,
  balance: { USDT: 7513.063625891424, VCOIN: 4545.454545454545 },
  buyAmount: 4545.454545454545,
  buyPrice: 0.66,
  totalUSDT: 10513.063625891424
}
>>>>> order {
  timestamp: '2022-01-22T08:17:00+07:00',
  datetime: 1642814220000,
  symbol: 'ONG/USDT',
  type: 'market',
  timeInForce: 'GTC',
  side: 'buy',
  price: 0.66,
  amount: 4545.454545454545,
  cost: 3000,
  average: 43394.61,
  filled: 0.002309,
  remaining: 0,
  status: 'closed'
}
>>>>> item1st {
  time: 1642814220000,
  timestamp: '2022-01-22T08:17:00+07:00',
  open: 0.6562,
  high: 0.6643,
  low: 0.6562,
  close: 0.66,
  volume: 10747,
  status: 'up',
  change: 0.5790917403230761,
  applitude: 1.2343797622676007,
  lockItem: 0,
  ma7: 0.6567428571428572,
  ma7OrderStt: 'normal'
}
Balance: VCOIN: 4545.454545454545, USDT: 7513.063625891424
Total USDT: 10513.063625891424. 


backlog 2: mua xong bán lỗ

-----------
>>>>> data  {
  progress: 'buying',
  progress_cmt: 'buying/selling',
  currentPrice: 0.6472,
  currentChart: 'normal',
  currentChart_cmt: 'normal/up/down',
  affterBuyHighPrice: 0.6472,
  affterBuyLowPrice: 0.6472,
  currentPercentAffterBuy: -2.348484848484844,
  priceSL: 0,
  buyType: 'du_dinh_volume',
  buyType_cmt: 'du_dinh/du_dinh_volume/du_dinh_volume_applitude/do_day/ko_do_day/du_dinh_co_node_giam',
  sellType: '',
  sellType_cmt: 'tp_up_on_top/tp_up_not_fast/tp_down/stop_loss',
  ma7: 0.6427142857142858,
  ma7OrderStt: 'up',
  chartTop: 0.834,
  chartBottom: 0.5732,
  chartTopHigh: 0.8209599999999999,
  chartTopLow: 0.80792,
  chartBottomHigh: 0.59928,
  chartBottomLow: 0.5862400000000001,
  balance: { USDT: 7442.609080436878, VCOIN: 4635.352286773795 },
  buyAmount: 4635.352286773795,
  buyPrice: 0.6472,
  totalUSDT: 10442.609080436878
}
>>>>> order {
  timestamp: '2022-01-22T08:45:00+07:00',
  datetime: 1642815900000,
  symbol: 'ONG/USDT',
  type: 'market',
  timeInForce: 'GTC',
  side: 'buy',
  price: 0.6472,
  amount: 4635.352286773795,
  cost: 3000,
  average: 43394.61,
  filled: 0.002309,
  remaining: 0,
  status: 'closed'
}
>>>>> item1st {
  time: 1642815900000,
  timestamp: '2022-01-22T08:45:00+07:00',
  open: 0.6435,
  high: 0.6475,
  low: 0.6435,
  close: 0.6472,
  volume: 3329,
  status: 'up',
  change: 0.5749805749805807,
  applitude: 0.6216006216006222,
  lockItem: 0,
  ma7: 0.6427142857142858,
  ma7OrderStt: 'normal'
}
Balance: VCOIN: 4635.352286773795, USDT: 7442.609080436878
Total USDT: 10442.609080436878. 
