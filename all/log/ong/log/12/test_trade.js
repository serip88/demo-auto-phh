const moment = require("moment");
const dalay = require('delay');
const config = require('./log/ong/item-config.json');
const data = require('./log/ong/data.json');
var { appDir } = require('../main');
const fs = require("fs");


const dataLog = fs.readFileSync('./all/log/log.json', 'utf8').split('\n')

const dataLogPure = dataLog.map(price => {
  price = JSON.parse(price)

  let format = []
  format.push(price.time)
  format.push(price.open)
  format.push(price.high)
  format.push(price.low)
  format.push(price.close)
  format.push(price.volume)
  return format
})

const TRADE_SIZE = config.TRADE_SIZE
const pairs = config.pairs
const tpMin = config.tpMin
const tpMax = config.tpMax
const stopLoss = config.stopLoss
const saleOnTopAcc = config.saleOnTopAcc

let progress = data.progress
let affterBuyHighPrice = data.affterBuyHighPrice
let affterBuyLowPrice = data.affterBuyLowPrice
let currentPercentAffterBuy = data.currentPercentAffterBuy
let currentPrice = data.currentPrice
let currentChart = data.currentChart
let priceSL = data.priceSL
let buyAmount = data.buyAmount
let buyPrice = data.buyPrice
let buyType = data.buyType

let chartTop = data.chartTop//120
let chartBottom = data.chartBottom//120

let chartBottomLow = data.chartBottomLow
let chartBottomHigh = data.chartBottomHigh
let chartTopHigh = data.chartTopHigh
let chartTopLow = data.chartTopLow

var item1st = false;
var item2nd = false;
var item3rd = false;
var item4th = false;
var item5th = false;
var item6th = false;

var lockItemsTime = [];

let level1m = 0;
let level2m = 0;
let level3m = 0;
let level4m = 0;
let level5m = 0;

//======= test param
let stopBot = false
let startItem = 0


async function mainHandle() {
  let isInit = await initItems();
  if (isInit) {
    await checkTrade();
  }
}

async function initTopBot() {
  let purePrices = dataLogPure;

  const hlPrices = purePrices.reduce((acc, price) => {
    acc.push(price[2])
    acc.push(price[3])
    return acc
  }, [])
  var min = Math.min.apply(null, hlPrices)
  var max = Math.max.apply(null, hlPrices)
  data.chartTop = max
  data.chartBottom = min

  console.log(">>>>min max", min, max)
  return hlPrices

}
async function initItems() {

  var step = 6
  var purePrices = dataLogPure.slice(startItem, startItem + step);
  if (purePrices.length < step) {
    stopBot = true
  } else if (startItem > 500) {
    stopBot = true
  }
  startItem += 1
  fPrices = await formatPrices(purePrices)

  item1st = fPrices[5];
  item2nd = fPrices[4];
  item3rd = fPrices[3];
  item4th = fPrices[2];
  item5th = fPrices[1];
  item6th = fPrices[0];

  itemTop = false
  itemBot = false


  //B handle set data
  data.currentPrice = item1st.close


  //set data.chartTop/bot
  if (data.currentPrice > data.chartTop) {
    data.chartTop = data.currentPrice
    itemTop = item1st
  }
  if (data.currentPrice < data.chartBottom) {
    data.chartBottom = data.currentPrice
    itemBot = item1st
  }

  data.currentChart = "normal"
  if (data.progress == "selling") {

    //set affterBuyHighPrice, affterBuyLowPrice affter buy
    if (data.currentPrice > data.affterBuyHighPrice) {
      data.affterBuyHighPrice = data.currentPrice
    }
    if (data.currentPrice < data.affterBuyLowPrice) {
      data.affterBuyLowPrice = data.currentPrice
    }

    // var aUnit = (data.affterBuyHighPrice - data.affterBuyLowPrice) / 100
    // data.currentPercentAffterBuy = (data.currentPrice - data.affterBuyLowPrice) / aUnit
    data.currentPercentAffterBuy = 0
    if(data.affterBuyHighPrice > data.buyPrice){
      var aUnit = (data.affterBuyHighPrice - data.buyPrice) / 100
      data.currentPercentAffterBuy = (data.currentPrice - data.buyPrice) / aUnit
    }
    //set currentChart
    if (data.currentPrice > data.buyPrice) {
      data.currentChart = "up"
    } else if (data.currentPrice < data.buyPrice) {
      data.currentChart = "down"
    }
  }
  //E handle set data

  // item1st.status = checkUpDown(item1st, item2nd)
  // item2nd.status = checkUpDown(item2nd, item3rd)
  // item3rd.status = checkUpDown(item3rd, item4th)
  // item4th.status = checkUpDown(item4th, item5th)
  // item5th.status = checkUpDown(item5th, item6th)

  let prices = []
  prices[0] = item1st
  prices[1] = item2nd
  level1m = await analysisAccelerate(prices);

  prices = []
  prices[0] = item1st
  prices[1] = item3rd
  level2m = await analysisAccelerate(prices);

  prices = []
  prices[0] = item1st
  prices[1] = item4th
  level3m = await analysisAccelerate(prices);

  prices = []
  prices[0] = item1st
  prices[1] = item5th
  level4m = await analysisAccelerate(prices);

  prices = []
  prices[0] = item1st
  prices[1] = item6th
  level5m = await analysisAccelerate(prices);
  console.log('level 0 >>>>', item1st)
  console.log('level 1 >>>>', level1m, item2nd)
  console.log('level 2 >>>>', level2m, item3rd)
  console.log('level 3 >>>>', level3m, item4th)
  console.log('level 4 >>>>', level4m, item5th)
  console.log('level 5 >>>>', level5m, item6th)

  return true
}

function checkUpDown(first, second) {

  var status = "normal"
  if (first.close > second.close) {
    status = "up"
  } else if (first.close < second.close) {
    status = "down"
  }
  return status
}

async function formatPrices(prices) {
  const bPrices = prices.map(price => {
    return {
      time: price[0],
      timestamp: moment(price[0]).format(),
      open: price[1],
      high: price[2],
      low: price[3],
      close: price[4],
      volume: price[5],
      status: price[1] < price[4] ? "up" : (price[1] > price[4] ? "down" : "normal"),
      change: (price[4] - price[1]) * 100 / price[1],
      applitude: (price[2] - price[3]) * 100 / price[3],
      lockItem: lockItemsTime.includes(moment(price[0]).format()) ? 1 : 0
    }
  })

  return bPrices
}
async function checkTrade() {

  //buying/selling
  data.chartBottomLow = (data.chartTop - data.chartBottom) / 100 * 5 + data.chartBottom
  data.chartBottomHigh = (data.chartTop - data.chartBottom) / 100 * 10 + data.chartBottom

  data.chartTopHigh = (data.chartTop - data.chartBottom) / 100 * 95 + data.chartBottom
  data.chartTopLow = (data.chartTop - data.chartBottom) / 100 * 90 + data.chartBottom
  console.log(" ---------- ")
  console.log(">>>>>> data.chartBottomHigh: ", data.chartBottomHigh)
  console.log(">>>>>> data.chartBottomLow: ", data.chartBottomLow)
  console.log(">>>>>> data.chartBottom: ", data.chartBottom)


  console.log(" ---------- ")
  console.log(">>>>>> data.chartTop: ", data.chartTop)
  console.log(">>>>>> data.chartTopHigh: ", data.chartTopHigh)
  console.log(">>>>>> data.chartTopLow: ", data.chartTopLow)
  console.log(" ---------- ")
  if (data.progress == "buying") {
    //item2nd.change > 0.4 bị dư, phòng trường hợp call api chậm, nếu fetch realtime thì bỏ qua
    if (item1st.lockItem == 0 && item2nd.lockItem == 0 && item1st.status == "up" && (item2nd.change > 0.4 || item1st.change > 0.4)) {
      //trường hợp tăng đột xuất, mua đu đỉnh
      //https://drive.google.com/file/d/11uQkq69c7Q8_RNeRE3OuE6mYFyBrrvvo/view?usp=sharing
      //https://drive.google.com/drive/folders/15RN3WceEFvMVZ3m90Ne3f4N1noDegfkz?ths=true
      data.buyType = "du_dinh"
      if(item1st.volume < (item2nd.volume * 2) && (item1st.volume > (item2nd.volume * 1.8)) && (item1st.applitude > (item2nd.applitude * 2)) && item1st.status == "up" && item2nd.status != "down" ){
        //fix case buy later
        //https://drive.google.com/drive/folders/1l9LxvW6pjkyalKHk0MXRPLNCSeOtBWCG?usp=sharing

        data.buyType = "du_dinh_volume_applitude"
        console.log(">>>>>> marketBuyLocal")
        order = await marketBuyLocal(item1st.close)

        logTrade.log(">>>>> data ", data)
        logTrade.log(">>>>> order", order)
        logTrade.log(">>>>> item1st", item1st)
        printBalance(item1st.close)
        updateBuyData()
      }else if(item1st.volume > (item2nd.volume * 2) ){
        data.buyType = "du_dinh_volume"
        console.log(">>>>>> marketBuyLocal")
        order = await marketBuyLocal(item1st.close)

        logTrade.log(">>>>> data ", data)
        logTrade.log(">>>>> order", order)
        logTrade.log(">>>>> item1st", item1st)
        printBalance(item1st.close)
        updateBuyData()
      }else if(item1st.lockItem == 0 && item2nd.lockItem == 0 && item1st.status == "up" && item2nd.status == "up" && item3rd.status == "down"  && (item2nd.change > 0.5 || item3rd.change < -1)){
        //update to check case giảm mạnh rồi tăng mạnh
        //https://drive.google.com/file/d/1Z7Z6jGftnzZKKfWZlv6cvh6I0UD_aTuu/view?usp=sharing
        data.buyType = "du_dinh_co_node_giam"
        console.log(">>>>>> marketBuyLocal")
        order = await marketBuyLocal(item1st.close)

        logTrade.log(">>>>> data ", data)
        logTrade.log(">>>>> order", order)
        logTrade.log(">>>>> item1st", item1st)
        printBalance(item1st.close)
        updateBuyData()

      }
      //}else if(item2nd.status == "up" && (item3rd.change < -0.4 || item4th.change < -0.4 || item5th.change < -0.4 ) && (item3rd.status == "down" || item4th.status == "down" || item5th.status == "down") && (data.chartBottomLow < data.currentPrice  && data.currentPrice < data.chartBottomHigh) ){
    } else if (item1st.lockItem == 0 && item2nd.lockItem == 0 && item2nd.status == "up" && (item3rd.change < -0.4 || item4th.change < -0.4 || item5th.change < -0.4) && (item3rd.status == "down" || item4th.status == "down" || item5th.status == "down") && (data.chartBottomLow < data.currentPrice)) {
      //item thu 2 có dấu hiệu tăng, item 3/4/5 giảm mạnh và current phải chạm đáy trong khoảng 90-95% -> trường hợp giảm liên tục, dò đáy
      //https://drive.google.com/file/d/1wtRLMCWd5va8YR5CdlCv-gjLVTxXO3R7/view?usp=sharing
      //https://drive.google.com/file/d/1IvNFPNgg7BjUxbqN5BiRQ02ZglIvXMQD/view?usp=sharing

      // tạm thời khóa vì trường hợp này lãi ít, có khả năng lỗ
      /*console.log(">>>>>> marketBuyLocal")
      data.buyType = "do_day"
      logTrade.log(">>>>> data ",data)
      order = await marketBuyLocal(item1st.close)
      
      logTrade.log(">>>>> order",order)
      logTrade.log(">>>>> item1st",item1st)
      printBalance(item1st.close)
      updateBuyData()*/
    } else if (item1st.lockItem == 0 && item2nd.lockItem == 0 && item2nd.status == "up" && (item3rd.change < -0.4 || item4th.change < -0.4 || item5th.change < -0.4) && (item3rd.status == "down" || item4th.status == "down" || item5th.status == "down")) {
      //item thu 2 có dấu hiệu tăng, item 3/4/5 giảm mạnh và current phải chạm đáy trong khoảng 90-95% -> trường hợp giảm liên tục, dò đáy
      //https://drive.google.com/file/d/1IvNFPNgg7BjUxbqN5BiRQ02ZglIvXMQD/view?usp=sharing
      console.log(">>>>>> marketBuyLocal")
      data.buyType = "ko_do_day"
      order = await marketBuyLocal(item1st.close)

      logTrade.log(">>>>> data ", data)
      logTrade.log(">>>>> order", order)
      logTrade.log(">>>>> item1st", item1st)
      printBalance(item1st.close)
      updateBuyData()

    }
  } else {//selling
    var dif = data.currentPrice - data.buyPrice
    var difPc = dif * 100 / data.buyPrice
    console.log(">>>>>>>>> difPc", difPc)
    //if(difPc >= config.tpMin && data.currentPercentAffterBuy < data.tpMax){//dif 2% tp and currentPercent < 95%
    if (difPc >= config.tpMin) {//dif 2%
      //TP
      //https://drive.google.com/file/d/1W0QWJQiGkfa6-S7fT7Dzkwfg-HhGqtsp/view?usp=sharing
      //https://drive.google.com/drive/folders/1n6qP25XFSPOTVvSLWn_aKIocDCpQHYx_?ths=true
      if (data.currentChart == "up") {//TP
        if (item1st.change > config.saleOnTopAcc || item2nd.change > config.saleOnTopAcc) {
          //tp_up_on_top acc fast so tp when data.currentPrice <= tpMax
          //tp_up_on_top check realtime, back test not exact for test this case
          //fast keep
          //if (data.currentPrice <= data.chartTopHigh) {//95% this cond have problem
          if (data.currentPercentAffterBuy <= config.tpMax) {//95%
            console.log(">>>>>> marketSellLocal")
            data.sellType = "tp_up_on_top"
            order = await marketSellLocal()

            logTrade.log(">>>>> data ", data)
            logTrade.log(">>>>> order", order)
            logTrade.log(">>>>> item1st", item1st)
            logTrade.log("TP difPc: ", difPc)
            printBalance(item1st.close)
            updateSellData()

          } else {// > 95%
            //on top, keep it
          }
        }else {
          if(data.currentPercentAffterBuy > config.tpMax ){
            //on top, , keep it
          }else{
            //tp_up_not_fast acc slow so tp when reach tp setup
            console.log(">>>>>> marketSellLocal")
            data.sellType = "tp_up_not_fast"
            order = await marketSellLocal()

            logTrade.log(">>>>> data ", data)
            logTrade.log(">>>>> order", order)
            logTrade.log(">>>>> item1st", item1st)
            logTrade.log("TP difPc: ", difPc)
            printBalance(item1st.close)
            updateSellData()
          } 
          

        }
      } else {
        console.log(">>>>>> marketSellLocal")
        data.sellType = "tp_down"
        order = await marketSellLocal()

        logTrade.log(">>>>> data ", data)
        logTrade.log(">>>>> order", order)
        logTrade.log(">>>>> item1st", item1st)
        logTrade.log("TP difPc: ", difPc)
        printBalance(item1st.close)
        updateSellData()

      }

    } else if (difPc <= config.stopLoss) {//-2
      //SL
      console.log(">>>>>> marketSellLocal")
      data.sellType = "stop_loss"
      order = await marketSellLocal()

      logTrade.log(">>>>> data ", data)
      logTrade.log(">>>>> order", order)
      logTrade.log(">>>>> item1st", item1st)
      logTrade.log("stopLoss difPc: ", difPc)
      printBalance(item1st.close)
      updateSellData()

    }

  }

  console.log(">>>>>>> data", data)
  console.log(">>>>>>> config", config)
  console.log(">>>>>>> startItem", startItem)

}

const { Console } = require("console");
const logTrade = new Console({
  stdout: fs.createWriteStream(appDir + "/log/ong/trade.log", { flags: 'a' }),
});

async function updateBuyData() {

  callBack = function (err) {
    if (err) throw err;
    console.log('updateBuyData callBack completed');
  }
  data.progress = "selling"

  fs.writeFile(appDir + "/log/ong/data.json", JSON.stringify(data), 'utf8', callBack);
  console.log(">>>> updateBuyData")

}

async function updateSellData() {

  callBack = function (err) {
    if (err) throw err;
    console.log('updateSellData callBack completed');
  }

  data.buyAmount = 0
  data.progress = "buying"
  data.buyPrice = 0
  data.affterBuyHighPrice = 0
  data.affterBuyLowPrice = 0
  data.buyType = ""

  fs.writeFile(appDir + "/log/ong/data.json", JSON.stringify(data), 'utf8', callBack);
  console.log(">>>> updateSellData")

}

async function printBalance(currentPrice) {

  const msg1 = `Balance: ONG ${data.balance.ONG}, USDT: ${data.balance.USDT}`
  data.totalUSDT = (data.balance.ONG) * currentPrice + data.balance.USDT
  const msg2 = `Total USDT: ${data.totalUSDT}. \n`
  console.log(msg1);
  console.log(msg2);

  logTrade.log(msg1)
  logTrade.log(msg2)
  logTrade.log("-----------")

}
async function marketBuyLocal(lastPrice) {

  const quantity = TRADE_SIZE / lastPrice
  const order = await createMarketOrderLocal(config.pairs, 'buy', quantity)
  return order
}
async function createMarketOrderLocal(pairs, mode, quantity) {

  var order = {
    timestamp: item1st.timestamp,
    datetime: item1st.time,
    symbol: config.pairs,
    type: 'market',
    timeInForce: 'GTC',
    side: mode,
    price: data.currentPrice,
    amount: quantity,
    cost: data.currentPrice * quantity,
    average: 43394.61,
    filled: 0.002309,
    remaining: 0,
    status: 'closed',
  }

  if (mode == "buy") {
    //nếu đủ tiền mới mua
    if (data.balance.USDT > TRADE_SIZE) {
      data.balance.USDT = data.balance.USDT - TRADE_SIZE
      data.balance.ONG = data.balance.ONG + order.amount
      data.affterBuyHighPrice = order.price
      data.affterBuyLowPrice = order.price
      data.sellType = ""
      data.buyAmount = order.amount
      data.buyPrice = order.price
    } else {
      logTrade.log("-----------")
      logTrade.log("Not enough USDT")
      return false
    }
  } else {
    data.balance.USDT = data.balance.USDT + order.cost
    data.balance.ONG = data.balance.ONG - order.amount
    data.buyType = ""
  }


  return order
}

async function marketSellLocal() {

  const quantity = data.buyAmount
  const order = await createMarketOrderLocal(config.pairs, 'sell', quantity)
  //B lock item
  lockItemsTime = []
  lockItemsTime.push(item1st.timestamp, item2nd.timestamp, item3rd.timestamp)
  //E lock item
  return order
}
async function analysisAccelerate(prices) {

  var hlPrices = await OHLCVformatHightLow(prices)

  var min = Math.min.apply(null, hlPrices)
  var max = Math.max.apply(null, hlPrices)

  let dif = max - min;

  let level = 0
  if (dif > 0) {
    level = (dif * 100) / min
  } else if (dif < 0) {
    level = (dif * 100) / max
  }
  return level
}


async function OHLCVformatHightLow(prices) {

  const hlPrices = prices.reduce((acc, price) => {
    acc.push(price.high)
    acc.push(price.low)
    return acc
  }, [])
  return hlPrices
}
async function loopLoop() {
  while (!stopBot) {
    await mainHandle();
    //await dalay(60* 1000);//60s
    await dalay(2 * 1000);//3s
  }
}

async function loopSolf() {
  //update gia tốc, currentPrice item1st mỗi 5s, để đảm bảo trong trường hợp giá tăng giảm mạnh có thể control
  //if != time tức là đã có nến mới, call mainHandle() if lastcall > 10s
}

//node all/test_trade.js

initTopBot()
//mainHandle()
loopLoop()
//await dalay(5* 1000);




