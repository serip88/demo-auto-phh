fix
buy:  timestamp: '2022-01-22T00:26:00+07:00',
https://drive.google.com/file/d/1TivFw7Vi0To6zv2vGE6WVglGAs2NG62r/view?usp=sharing

sell: timestamp: '2022-01-22T01:51:00+07:00',
https://drive.google.com/file/d/1apjUQDbl5xTA-1KOHVEQFomK7nqYctMM/view?usp=sharing

-> fix trường hợp sell
 data.affterBuyHighPrice: 4.993,
 -> check lấy từ high chứ ko lấy từ close, vì api chạy có delay
=> giá trần lúc này phải là: 5.12
-> check trường hợp ngoại lệ bán trước ma7OrderStt down, vì nó đang push mạnh


-----------
>>>>> data  {
  progress: 'buying',
  progress_cmt: 'buying/selling',
  currentPrice: 4.681,
  currentChart: 'normal',
  currentChart_cmt: 'normal/up/down',
  affterBuyHighPrice: 4.681,
  affterBuyLowPrice: 4.681,
  currentPercentAffterBuy: -2.8701218253148966,
  priceSL: 0,
  buyType: 'du_dinh_co_node_giam',
  buyType_cmt: 'du_dinh/du_dinh_volume/du_dinh_volume_applitude/do_day/ko_do_day/du_dinh_co_node_giam',
  sellType: '',
  sellType_cmt: 'tp_up_on_top/tp_up_not_fast/tp_down/stop_loss',
  ma7: 4.6532857142857145,
  ma7OrderStt: 'up',
  chartTop: 7.22,
  chartBottom: 4.052,
  chartTopHigh: 7.061599999999999,
  chartTopLow: 6.9032,
  chartBottomHigh: 4.368799999999999,
  chartBottomLow: 4.2104,
  balance: { USDT: 6925.921289380707, VCOIN: 640.888698995941 },
  buyAmount: 640.888698995941,
  buyPrice: 4.681,
  totalUSDT: 9925.921289380707
}
>>>>> order {
  timestamp: '2022-01-22T00:26:00+07:00',
  datetime: 1642785960000,
  symbol: 'API3/USDT',
  type: 'market',
  timeInForce: 'GTC',
  side: 'buy',
  price: 4.681,
  amount: 640.888698995941,
  cost: 3000,
  average: 43394.61,
  filled: 0.002309,
  remaining: 0,
  status: 'closed'
}
>>>>> item1st {
  time: 1642785960000,
  timestamp: '2022-01-22T00:26:00+07:00',
  open: 4.665,
  high: 4.683,
  low: 4.658,
  close: 4.681,
  volume: 4778.51,
  status: 'up',
  change: 0.3429796355841375,
  applitude: 0.5367110347788636,
  lockItem: 0,
  ma7: 4.6532857142857145,
  ma7OrderStt: 'normal'
}
Balance: VCOIN: 640.888698995941, USDT: 6925.921289380707
Total USDT: 9925.921289380707. 

-----------
>>>>> data  {
  progress: 'selling',
  progress_cmt: 'buying/selling',
  currentPrice: 4.908,
  currentChart: 'up',
  currentChart_cmt: 'normal/up/down',
  affterBuyHighPrice: 4.993,
  affterBuyLowPrice: 4.62,
  currentPercentAffterBuy: 72.75641025641029,
  priceSL: 0,
  buyType: '',
  buyType_cmt: 'du_dinh/du_dinh_volume/du_dinh_volume_applitude/do_day/ko_do_day/du_dinh_co_node_giam',
  sellType: 'tp_up_not_fast',
  sellType_cmt: 'tp_up_on_top/tp_up_not_fast/tp_down/stop_loss',
  ma7: 4.915571428571428,
  ma7OrderStt: 'down',
  chartTop: 7.22,
  chartBottom: 4.052,
  chartTopHigh: 7.061599999999999,
  chartTopLow: 6.9032,
  chartBottomHigh: 4.368799999999999,
  chartBottomLow: 4.2104,
  balance: { USDT: 10071.403024052786, VCOIN: 0 },
  buyAmount: 640.888698995941,
  buyPrice: 4.681,
  totalUSDT: 9925.921289380707
}
>>>>> order {
  timestamp: '2022-01-22T01:51:00+07:00',
  datetime: 1642791060000,
  symbol: 'API3/USDT',
  type: 'market',
  timeInForce: 'GTC',
  side: 'sell',
  price: 4.908,
  amount: 640.888698995941,
  cost: 3145.481734672079,
  average: 43394.61,
  filled: 0.002309,
  remaining: 0,
  status: 'closed'
}
>>>>> item1st {
  time: 1642791060000,
  timestamp: '2022-01-22T01:51:00+07:00',
  open: 4.997,
  high: 5.01,
  low: 4.908,
  close: 4.908,
  volume: 10654.46,
  status: 'down',
  change: -1.7810686411847014,
  applitude: 2.078239608801944,
  lockItem: 0,
  ma7: 4.915571428571428,
  ma7OrderStt: 'normal'
}
TP difPc:  4.849391155735961
Balance: VCOIN: 0, USDT: 10071.403024052786
Total USDT: 10071.403024052786. 
