//lưu lại data của 1 cặp để chạy backtest

const ccxt = require("ccxt");
const moment = require("moment");
const dalay = require("delay");

var { config } = require("../main");

const binance = new ccxt.binance({
  apiKey: config.API_R,
  secret: config.SECRET_R,
});
//binance.setSandboxMode(true)

async function getLastPrice() {
  const prices = await binance.fetchOHLCV("STORJ/USDT", "1m", undefined, 500);
  let lastPrice = 0;

  const bPrices = prices.map((price) => {
    //lastPrice = price[4]
    //console.log(lastPrice );
    var formatItem = {
      timestamp: moment(price[0]).format(),
      time: price[0],
      open: price[1],
      high: price[2],
      low: price[3],
      close: price[4],
      volume: price[5],
      status:
        price[1] < price[4] ? "up" : price[1] > price[4] ? "down" : "normal",
      change: ((price[4] - price[1]) * 100) / price[1],
      applitude: ((price[2] - price[3]) * 100) / price[3],
    };
    console.log(JSON.stringify(formatItem));
  });
}

//node all/get_data_log.js | tee -a all/log/storj/log-2022-06-19_500.txt
//cần thay thế dòng 16 và 39
//hàm cho phép lấy log của 1 coin
getLastPrice();
